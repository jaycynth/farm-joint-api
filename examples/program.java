import Newtonsoft.Json;
import Newtonsoft.Json.Linq;
import Org.BouncyCastle.Crypto;
import Org.BouncyCastle.Crypto.Encodings;
import Org.BouncyCastle.Crypto.Engines;
import Org.BouncyCastle.Crypto.Parameters;
import Org.BouncyCastle.OpenSsl;
import System;
import System.Reflection;
import System.Collections.Generic;
import System.IO;
import System.Linq;
import System.Net;
import System.Runtime.InteropServices;
import System.Security.Cryptography;
import System.Text;
import System.Text.RegularExpressions;
import System.Threading.Tasks;

namespace Test {
    
    class Program {
        
        public static string requestData = "";
        
        //  * URL
        public static string url = "http://196.28.226.22:8080/iface/index";
        
        // Test http://196.28.226.22:8080/iface/index  //Prod http://196.28.226.18:8080/iface/index
        //  * Taxpayer Info
        public static string[] taxpayerInfo;
        
        // NUIT
        //  * EFD/Server Info
        public static string[] deviceInfo;
        
        public static string[] 100010001033;
        
        public static string[] 010400000033;
        
        public static string[] S2S;
        
        // License & SN & Code & Model
        public static string privateKey = @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJ/kKJDe5Ti+cXy/L9pJ4hTmuIIZ14XCoQkrIax7+DBRL7Hz2Ojz27fW7k3Cus80pQLt7eauWxu0MkTTvLaB5o3dzEMzstRDedHVYoD52n+6wqwXpgLx35ApRnkpdk7WJc+RcjroB9lfTmTmQ9Ejtk2kRCBnzF+nF7jI4THic441AgMBAAECgYAnCuyI65SndYF6x6iksfPdzLTzN+IzzGHMPlb7qk3hZoGx/2xQ/fMCAh6wqEM7g4xjmbvE34Bp1GeN/PIQ0sl8KftwNHTIfhnAXVnOS/8YJq8Is5N+ZbeX7JPAUHjHTcZkWLj7uNAjrIqxExN+L0Q869J+U5tannmcam/d/8W8RQJBANAWS7L9gqMrF5nMr7vE0nWAVGSp2cTf5l4dqwF1RNjRRxbvXpxQrIjkLZfg335BH3id1cekW2Qjt+y7X5HJ778CQQDEtPS8qkw4mFaV9y5Eq093ugq+a3PQfPk3Sc8QhNufFKvQaOgCQm/vH9oktIpbh2HGnzPUCimiHZjpIf3Eyf8LAkEAzQxrY+G0DZ21D/VHZjw5N1hR0TAeS4Zmt298x18LjKwM5WjTTi2hFvPiibsqAHujy3Oc6bHn7PHVmA27yym2eQJAcvM0WdRC+oe7gaOS/DoIFHU5thFg+qMtl+uf/41EMnCH/yBvBHMCqoC4n96JQ/eXYdTDGtRdhAk7xHFCtO8+BQJAXKh1BOzT2hoQMX+sv5alNB1EhuJ1Oyastl3yNHWmWVvPBK8uggZKRzdRQuelS2vvszgYHdruHk7vhHUXLE+n1w==";
        
        //  * Invoice Info
        public static string[] invoiceInfo;
        
        public static string[] deviceInfo;
        
        public static string[] 000180110000;
        
        public static string[] 06103666;
        
        public static string[] 117.00;
        
        public static string[] 2019-08-18 15:25:15;
        
        // NUIT & Device Code & Invoice Code & Invoice No. & Total Amount & Invoicing Time
        // Business ID
        public static string[] busID;
        
        public static string[] R-R-02;
        
        public static string[] R-R-03;
        
        public static string[] INFO-MODI-R;
        
        public static string[] INVOICE-APP-R;
        
        public static string[] INVOICE-REPORT-R;
        
        public static string[] INVOICE-RETRIEVE-R;
        
        public static string[] SYS-TIME-R;
        
        public static string[] UPDATE-IP-R;
        
        public static string[] MONITOR-R;
        
        public static string[] ALARM-R;
        
        public static string[] RECOVER-R;
        
        static void Main(string[] args) {
            Console.WriteLine("\nEnter the business ID\n0 --> R-R-01\n1 --> R-R-02 \n2 --> R-R-03 \n3 --> INFO-MODI-R\n4 --> INVOICE-APP-R" +
                "\n5 --> INVOICE-REPORT-R\n6 --> INVOICE-RETRIEVE-R\n7 --> SYS-TIME-R\n8 --> UPDATE-IP-R\n9 --> MONITOR-R\n" +
                "10 --> ALARM-R\n11 --> RECOVER-R");
            var bus_id = Console.ReadLine();
            Program.IMSMain(bus_id);
        }
        
        public static void IMSMain(string bus_id) {
            try {
                var bus_content = "";
                if ("R-R-01".Equals(bus_id)) {
                    //  [Initialization - 1.Private Key Application]
                    bus_content = "{\"license\":\"\" + deviceInfo[0] + ";
                    ",\"sn\":\"\" + deviceInfo[1] + ";
                    ",\"sw_version\":\"10\",\"model\":\"\" + deviceInfo[3] + ";
                    ",\"manufacture\":\"Inspur Software Group\",\"imei\":\"\\";
                    "os\":\"\\";
                    "hw_sn\":\"\\";
                }
                
                "";
                // bus_content = "{\"license\": \"821255901354\",\"sn\": \"2187603000010\",\"sw_version\": \"2019.0\",\"model\": \"IP - 100\",\"manufacture\": \"GrupoPIE\",\"imei\": \"\",\"os\": \"linux2 .6 .36\",\"hw_sn\": \"123456789\"}";
            }
            
            if ("INVOICE-REPORT-R".Equals(bus_id)) {
                //  [Invoice upload]
                var fiscalCode = GetFiscalCode();
                DateTime startTime = new DateTime(1970, 1, 1);
                DateTime endTime = Convert.ToDateTime(invoiceInfo[5]).AddHours(-2);
                // UTC+2
                long invocingDate = ((long)((endTime - startTime).TotalSeconds));
                bus_content = "{\"id\":\"\" + invoiceInfo[1] + ";
                ",\"POS-SN\":\"\\";
                "declaration-info\":{\"invoice-code\":\"\" + invoiceInfo[2] + ";
                ",\"invoice-number\":\"\" + invoiceInfo[3] + ";
                ",\"buyer-tpin\":\"\\";
                "buyer-vat-acc-name\":\"\\";
                " ";
                "\"buyer-name\":\"\\";
                "buyer-address\":\"\\";
                "buyer-tel\":\"\\";
                ("tax-amount\":17,\"total-amount\":" 
                            + (invoiceInfo[4] + (",\"total-discount\":0," + ("\"invoice-status\":\"01\",\"invoice-issuer\":\"Administrator\",\"invoicing-time\":" 
                            + (Convert.ToString(invocingDate) + ",\"old-invoice-code\":\"\\")))));
                "old-invoice-number\":\"\\";
                " ";
                "\"fiscal-code\":\"\" + fiscalCode + ";
                ",\"memo\":\"\\";
                "sale-type\":0,\"currency-type\":\"USD\",\"conversion-rate\":1,\"local-purchase-order\":\"\\";
                "voucher-PIN\":\"\\";
                " ";
                "\"items-info\":[{\"no\":\"1\",\"tax-category-code\":\"A\",\"tax-category-name\":\"IVA\",\"name\":\"Cinta\",\"barcode\":\"\\" +
                "";
                ("count\":10,\"amount\":100,\"tax-amount\":17," + "\"discount\":0,\"unit-price\":10,\"tax-rate\":0.17,\"rrp\":0}],\"tax-info\":[{\"tax-code\":\"A\",\"tax-name\":\"IVA\",\"" +
                "tax-rate\":0.17,\"tax-value\":17}]}}");
            }
            else if ("INVOICE-RETRIEVE-R".Equals(bus_id)) {
                //  [Invoice Query]
                bus_content = "{\"id\":\"\" + deviceInfo[2] + ";
                ",\"code\":\"000180310000\",\"number\":\"00001238\"}";
            }
            else if ("MONITOR-R".Equals(bus_id)) {
                //  [Heartbeat Monitoring]
                bus_content = "{\"id\":\"\" + deviceInfo[2] + ";
                ",\"batch\":\"20180311001\",\"lon\":0,\"lat\":0,\"sw_version\":\"1.5\"}";
            }
            else if ("ALARM-R".Equals(bus_id)) {
                //  [Alarm Notification]
                bus_content = "{\"id\":\"\" + deviceInfo[2] + ";
                ",\"level\":\"02\",\"info\":\"2586336655566655\"}";
            }
            else {
                //  [Initialization - 2.Tax Information Application] & [Initialization - 3.Initialization Success Notification] & [Tax information modification] & [Invoice Section application] & [Time Synchronization] & [Server IP Amendment] & [Reactivation]
                bus_content = "{\"id\":\"\" + deviceInfo[2] + ";
                "}";
            }
            
            Console.WriteLine(("Bus Content: " + bus_content));
            var randomK = deviceInfo[0].Substring((deviceInfo[0].Length - 8));
            // TODO
            byte[] key = ASCIIEncoding.ASCII.GetBytes(randomK);
            var enKey = RsaEncryptWithPrivateKey(privateKey, key);
            var content = DesEncrypt(bus_content, key);
            var sign = Sign(content);
            string requestData = "";
            if (!bus_id.Equals("R-R-01")) {
                requestData = "{\"message\":{\"body\":{\"data\":{\"device\":\"\" + deviceInfo[2] + ";
                ",\"serial\":\"688877\",\"sign\":\"\" + sign + ";
                ",\"key\":\"\" + enKey + ";
                ",\"bus_id\":\"\" + bus_id + ";
                ",\"content\":\"\" + content + ";
                "}}}}";
            }
            else {
                requestData = "{\"message\":{\"body\":{\"data\":{\"device\":\"\" + deviceInfo[0] + ";
                ",\"serial\":\"688877\",\"sign\":\"\" + sign + ";
                ",\"key\":\"\\";
                "bus_id\":\"\" + bus_id + ";
                ",\"content\":\"\" + content + ";
                "}}}}";
            }
            
            Console.WriteLine("*****************************************************************************************************" +
                "******************");
            Console.WriteLine(("reqData: " + requestData));
            Console.WriteLine("*****************************************************************************************************" +
                "******************");
            var responeData = DoPostHttp(requestData, url);
            Console.WriteLine(("\r\nresponeData:" + responeData));
            Console.WriteLine("*****************************************************************************************************" +
                "******************");
            JObject jo = ((JObject)(JsonConvert.DeserializeObject(responeData)));
            JObject joMessage = ((JObject)(JsonConvert.DeserializeObject(jo["message"].ToString())));
            JObject joBody = ((JObject)(JsonConvert.DeserializeObject(joMessage["body"].ToString())));
            JObject joData = ((JObject)(JsonConvert.DeserializeObject(joBody["data"].ToString())));
            var responeKey = joData["key"].ToString();
            if (!bus_id.Equals("R-R-01")) {
                key = RsaDecryptWithPrivateKey(responeKey, privateKey);
            }
            
            var responeContent = joData["content"].ToString();
            var result = DesDecrypt(responeContent, key);
            Console.WriteLine("*****************************************************************************************************" +
                "******************");
            Console.WriteLine(("\r\nresKey: " + responeKey));
            Console.WriteLine(("\r\nresContent: " + responeContent));
            Console.WriteLine(("\r\nresMessage: " + result));
        }
    }
}
#
region Obsolete;
Codes;
#
endregion Obsolete;
Codes;
UnknownUnknown
    
    private static string DesDecrypt(string cryptedString, byte[] keyBytes) {
        if (((keyBytes == null) 
                    || (keyBytes.Length == 0))) {
            throw new ArgumentNullException("The string which needs to be encrypted can not be null.");
        }
        
        DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
        cryptoProvider.Mode = CipherMode.ECB;
        cryptoProvider.Padding = PaddingMode.Zeros;
        MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
        CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(keyBytes, keyBytes), CryptoStreamMode.Read);
        StreamReader reader = new StreamReader(cryptoStream);
        return reader.ReadToEnd();
    }
    
    private static string DesEncrypt(string plainText, byte[] keyBytes) {
        if (((keyBytes == null) 
                    || (keyBytes.Length == 0))) {
            throw new ArgumentNullException("The string which needs to be encrypted can not be null.");
        }
        
        DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
        cryptoProvider.Mode = CipherMode.ECB;
        cryptoProvider.Padding = PaddingMode.Zeros;
        MemoryStream memoryStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(keyBytes, keyBytes), CryptoStreamMode.Write);
        StreamWriter writer = new StreamWriter(cryptoStream);
        writer.Write(plainText);
        writer.Flush();
        cryptoStream.FlushFinalBlock();
        writer.Flush();
        return Convert.ToBase64String(memoryStream.GetBuffer(), 0, ((int)(memoryStream.Length)));
    }
    
    private static string Sign(string content) {
        MD5 md5Hash = MD5.Create();
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(content));
        return Convert.ToBase64String(data);
    }
    
    private static AsymmetricKeyParameter ReadPrivateKey(string privateKey) {
        AsymmetricKeyParameter asymKey;
        TextReader sr = new StringReader("-----BEGIN PRIVATE KEY-----\n{privateKey}\n-----END PRIVATE KEY-----");
        asymKey = ((AsymmetricKeyParameter)(new PemReader(sr))).ReadObject();
        return asymKey;
    }
    
    public static string RsaEncryptWithPrivateKey(string privateKey, string textToEncrypt) {
        AsymmetricKeyParameter asymKey = ReadPrivateKey(privateKey);
        var keyPair = GetKeyPairFromPrivateKey(asymKey);
        IAsymmetricBlockCipher engine = new Pkcs1Encoding(new RsaEngine());
        engine.Init(true, keyPair.Public);
        byte[] cipheredBytes = engine.ProcessBlock(Encoding.ASCII.GetBytes(textToEncrypt), 0, textToEncrypt.Length);
        return Convert.ToBase64String(cipheredBytes);
    }
    
    public static string RsaEncryptWithPrivateKey(string privateKey, byte[] keyBytes) {
        AsymmetricKeyParameter asymKey = ReadPrivateKey(privateKey);
        var keyPair = GetKeyPairFromPrivateKey(asymKey);
        IAsymmetricBlockCipher engine = new Pkcs1Encoding(new RsaEngine());
        engine.Init(true, keyPair.Private);
        byte[] cipheredBytes = engine.ProcessBlock(keyBytes, 0, keyBytes.Length);
        return Convert.ToBase64String(cipheredBytes);
    }
    
    public static byte[] RsaDecryptWithPrivateKey(string base64String, string privateKey) {
        var bytesToDecrypt = Convert.FromBase64String(base64String);
        AsymmetricKeyParameter asymKey = ReadPrivateKey(privateKey);
        var keyPair = GetKeyPairFromPrivateKey(asymKey);
        var decryptEngine = new Pkcs1Encoding(new RsaEngine());
        decryptEngine.Init(false, keyPair.Private);
        return decryptEngine.ProcessBlock(bytesToDecrypt, 0, bytesToDecrypt.Length);
    }
    
    private static AsymmetricCipherKeyPair GetKeyPairFromPrivateKey(AsymmetricKeyParameter privateKey) {
        AsymmetricCipherKeyPair keyPair = null;
        if ((privateKey instanceof RsaPrivateCrtKeyParameters)) {
            rsa;
        }
        
        var pub = new RsaKeyParameters(false, rsa.Modulus, rsa.PublicExponent);
        keyPair = new AsymmetricCipherKeyPair(pub, privateKey);
        if ((privateKey instanceof Ed25519PrivateKeyParameters)) {
            ed;
        }
        
        var pub = ed.GeneratePublicKey();
        keyPair = new AsymmetricCipherKeyPair(pub, privateKey);
        if ((privateKey instanceof ECPrivateKeyParameters)) {
            ec;
        }
        
        var q = ec.Parameters.G.Multiply(ec.D);
        var pub = new ECPublicKeyParameters(ec.AlgorithmName, q, ec.PublicKeyParamSet);
        keyPair = new AsymmetricCipherKeyPair(pub, ec);
        if ((keyPair == null)) {
            throw new NotSupportedException("The key type {privateKey.GetType().Name} is not supported.");
        }
        
        return keyPair;
    }
    
    private static string Encrypt(string plainText, string key) {
        if ((String.IsNullOrEmpty(plainText) || String.IsNullOrEmpty(key))) {
            throw new ArgumentNullException("The string which needs to be encrypted can not be null.");
        }
        
        byte[] bytes = ASCIIEncoding.ASCII.GetBytes(key);
        DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
        cryptoProvider.Mode = CipherMode.ECB;
        cryptoProvider.Padding = PaddingMode.Zeros;
        MemoryStream memoryStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);
        StreamWriter writer = new StreamWriter(cryptoStream);
        writer.Write(plainText);
        writer.Flush();
        cryptoStream.FlushFinalBlock();
        writer.Flush();
        return Convert.ToBase64String(memoryStream.GetBuffer(), 0, ((int)(memoryStream.Length)));
    }
    
    private static string Decrypt(string cryptedString, string key) {
        if ((String.IsNullOrEmpty(cryptedString) || String.IsNullOrEmpty(key))) {
            throw new ArgumentNullException("The string which needs to be decrypted can not be null.");
        }
        
        byte[] bytes = ASCIIEncoding.ASCII.GetBytes(key);
        DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
        cryptoProvider.Mode = CipherMode.ECB;
        cryptoProvider.Padding = PaddingMode.Zeros;
        MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
        CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
        StreamReader reader = new StreamReader(cryptoStream);
        return reader.ReadToEnd();
    }
    
    private static Stream GenerateStreamFromString(string s) {
        MemoryStream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }
    
    public static string DoPostHttp(string requestData, string url) {
        HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(url)));
        request.Method = "POST";
        request.ContentType = "application/json";
        StreamWriter dataStream = new StreamWriter(request.GetRequestStream());
        dataStream.Write(requestData);
        dataStream.Close();
        HttpWebResponse response = ((HttpWebResponse)(request.GetResponse()));
        string encoding = response.ContentEncoding;
        if (((encoding == null) 
                    || (encoding.Length < 1))) {
            encoding = "UTF-8";
        }
        
        StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(encoding));
        string retString = reader.ReadToEnd();
        return retString;
    }
    
    public static string GetFiscalCode() {
        StringBuilder fiscalCode = new StringBuilder();
        var bytesPrivateKey = Convert.FromBase64String(privateKey);
        var NUIT = invoiceInfo[0].PadLeft(18, '0');
        var deviceCode = invoiceInfo[1].PadLeft(12, '0');
        var invoiceCode = invoiceInfo[2].PadLeft(12, '0');
        var invoiceNo = invoiceInfo[3].PadLeft(8, '0');
        var totalAmount = invoiceInfo[4].PadLeft(20, '0');
        var invoicingDate = invoiceInfo[5].Replace("-", "").Replace(" ", "").Replace(":", "").PadLeft(14, '0');
        GetFiscalCode(NUIT, invoiceCode, invoiceNo, invoicingDate, deviceCode, totalAmount, fiscalCode, bytesPrivateKey, bytesPrivateKey.Length);
        return fiscalCode.ToString().Substring(0, 20);
    }
    
    @DllImport("libGetFiscalCode.so", CallingConvention=CallingConvention.Cdecl, EntryPoint="GetFiscalCode", SetLastError=true)
    static extern int GetFiscalCode(string NUIT, string InvoiceCode, string InvoiceNo, string InvoicingDate, string TerminalID, string TotalAmount, StringBuilder fiscalCode, byte[] priKey, int keyLen);