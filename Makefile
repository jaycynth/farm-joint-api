PROJECT_NAME := farm-joint
PROJECT_ROOT := ./ 
PKG := github.com/jaycynth/$(PROJECT_NAME)
SERVICE_CMD_FOLDER := ${PKG}/cmd
API_IN_PATH := api/proto
API_OUT_PATH := pkg/api
OPENAPIV2_DOC_OUT_PATH := api/swagger

setup_dev: ## Sets up a development environment for the project
	@cd deployments/compose &&\
	docker-compose up -d

teardown_dev: ## Tear down development environment for the emrs project
	@cd deployments/compose &&\
	docker-compose down

	
protoc_produce:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/produce --go_opt=paths=source_relative --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/produce --go-grpc_opt=paths=source_relative produce.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/produce produce.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) produce.proto

protoc_farm_animal:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/farmanimal --go_opt=paths=source_relative --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/farmanimal --go-grpc_opt=paths=source_relative farmanimal.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/farmanimal farmanimal.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) farmanimal.proto

protoc_health:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/health --go_opt=paths=source_relative --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/health --go-grpc_opt=paths=source_relative health.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/health health.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) health.proto

protoc_purchase:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/purchase --go_opt=paths=source_relative --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/purchase --go-grpc_opt=paths=source_relative purchase.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/purchase purchase.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) purchase.proto

protoc_payment:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/payment --go_opt=paths=source_relative --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/payment --go-grpc_opt=paths=source_relative payment.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/payment payment.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) payment.proto

protoc_feed:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/feed --go_opt=paths=source_relative --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/feed --go-grpc_opt=paths=source_relative feed.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/feed feed.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) feed.proto

protoc_supplement:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/supplement --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/supplement --go-grpc_opt=paths=source_relative supplement.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/supplement supplement.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) supplement.proto

protoc_sale:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/sale --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/sale --go-grpc_opt=paths=source_relative sale.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/sale sale.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) sale.proto

protoc_user:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=$(API_OUT_PATH)/user --go_opt=paths=source_relative --go-grpc_out=$(API_OUT_PATH)/user --go-grpc_opt=paths=source_relative user.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/user user.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --openapiv2_out=logtostderr=true,repeated_path_param_separator=ssv:$(OPENAPIV2_DOC_OUT_PATH) user.proto

protoc_all: protoc_produce protoc_health protoc_sale protoc_farm_animal protoc_purchase protoc_payment protoc_feed protoc_supplement protoc_user

cp_doc:
	@cp -r $(OPENAPIV2_DOC_OUT_PATH)/ cmd/apidoc/dist/swagger/

gen_api_doc: protoc_all cp_doc

run_apidoc:
	@cd ./cmd/apidoc && go run *.go

start_apidoc: protoc_all cp_doc run_apidoc