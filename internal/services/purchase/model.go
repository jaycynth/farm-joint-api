package purchase

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const purchaseTable = "purchases"

// Purchase ... (Billing) (metadata about payment and object being paid for)
type Purchase struct {
	PurchaseID uint   `gorm:"primaryKey;autoIncrement"`
	ItemID     string // farmanimal_id or produce_id
	ItemType   string // FARM_ANIMAL or PRODUCE
	PaymentID  string
	UserID     string
	ItemCost   float32
	CreatedAt  time.Time
}

// TableName ...
func (*Purchase) TableName() string {
	return purchaseTable
}

// https: (http2) -> support grpc
// http: (http1.1) -> no support for grpc
// create a grpc server, the gateway forward request to server

// GetPurchaseDB ...
func GetPurchaseDB(purchasePB *purchase.Purchase) (*Purchase, error) {
	if purchasePB == nil {
		return nil, status.Error(codes.InvalidArgument, "missing purchase PB in model")
	}

	purchaseDB := &Purchase{
		ItemID:    purchasePB.GetItemId(),
		ItemType:  purchasePB.GetItemType().String(),
		PaymentID: purchasePB.GetPaymentId(),
		UserID:    purchasePB.GetUserId(),
		ItemCost:  purchasePB.GetItemCost(),
	}

	return purchaseDB, nil
}

// GetPurchasePB ...
func GetPurchasePB(purchaseDB *Purchase) (*purchase.Purchase, error) {
	if purchaseDB == nil {
		return nil, status.Error(codes.InvalidArgument, "missing purchase DB in model")
	}

	purchasePB := &purchase.Purchase{
		PurchaseId: fmt.Sprint(purchaseDB.PurchaseID),
		ItemId:     purchaseDB.ItemID,
		ItemType:   purchase.ItemType(purchase.ItemType_value[purchaseDB.ItemType]),
		PaymentId:  purchaseDB.PaymentID,
		UserId:     purchaseDB.UserID,
		ItemCost:   purchaseDB.ItemCost,
	}

	return purchasePB, nil
}
