package purchase

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type purchaseAPIServer struct {
	purchase.UnimplementedPurchaseAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewPurchaseAPI creates a purchase singleton service
func NewPurchaseAPI(ctx context.Context, opt *option.Options) (purchase.PurchaseAPIServer, error) {
	// Validation
	switch {
	case ctx == nil:
		return nil, errors.New("context must not be nil")
	case opt == nil:
		return nil, errors.New("options is required")
	case opt.SQLDB == nil:
		return nil, errors.New("sql database is required")
	case opt.RedisDB == nil:
		return nil, errors.New("redis database is required")
	case opt.Logger == nil:
		return nil, errors.New("logger should not be nil")
	case opt.APIHashKey == "":
		return nil, errors.New("missing api hash key")
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &purchaseAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	if !api.SQLDB.Migrator().HasTable(purchaseTable) {
		err = api.SQLDB.Migrator().AutoMigrate(&Purchase{})
		if err != nil {
			return nil, err
		}
	}

	return api, nil
}

func (purchaseAPI *purchaseAPIServer) CreatePurchase(
	ctx context.Context, req *purchase.CreatePurchaseRequest,
) (*purchase.Purchase, error) {
	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	case req.Purchase == nil:
		return nil, status.Error(codes.InvalidArgument, "missing purchase")
	case req.Purchase.ItemId == "":
		return nil, status.Error(codes.InvalidArgument, "missing item id")
	case req.Purchase.ItemType == purchase.ItemType_ITEM_TYPE_UNSPECIFIED:
		return nil, status.Error(codes.InvalidArgument, "missing item type")
	}

	purchaseDB, err := GetPurchaseDB(req.Purchase)
	if err != nil {
		return nil, err
	}

	// Save to db
	err = purchaseAPI.SQLDB.Create(purchaseDB).Error
	if err != nil {
		purchaseAPI.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to save purchase")
	}

	return GetPurchasePB(purchaseDB)
}

func (purchaseAPI *purchaseAPIServer) GetPurchase(
	ctx context.Context, req *purchase.GetPurchaseRequest,
) (*purchase.Purchase, error) {
	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	case req.PurchaseId == "":
		return nil, status.Error(codes.InvalidArgument, "purchase id")
	}

	purchaseDB := &Purchase{}

	// Get from database
	err := purchaseAPI.SQLDB.First(purchaseDB, "purchase_id=?", req.PurchaseId).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "purchase does not exist")
	default:
		purchaseAPI.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "getting purchase failed")
	}

	return GetPurchasePB(purchaseDB)
}

const defaultPageSize = 50

func (purchaseAPI *purchaseAPIServer) ListPurchases(
	ctx context.Context, listReq *purchase.ListPurchasesRequest,
) (*purchase.ListPurchasesResponse, error) {

	payload, errr := purchaseAPI.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}
	// Validation
	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	}

	var err error

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var purchaseID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := purchaseAPI.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		purchaseID = uint(ids[0])
	}

	purchaseSrvDBs := make([]*Purchase, 0, pageSize)

	db := purchaseAPI.SQLDB.Limit(int(pageSize - 1)).Order("purchase_id DESC")

	if pageToken != "" {
		db = db.Where("purchase_id<?", purchaseID)
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	// Querying for many resources
	err = db.Find(&purchaseSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list purchases from database")
	}

	purchaseSrvPBs := make([]*purchase.Purchase, 0, len(purchaseSrvDBs))

	for _, purchaseSrvDB := range purchaseSrvDBs {
		purchaseSrvPB, err := GetPurchasePB(purchaseSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		purchaseSrvPBs = append(purchaseSrvPBs, purchaseSrvPB)
		purchaseID = purchaseSrvDB.PurchaseID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(purchaseSrvDBs) {
		// Next page token
		token, err = purchaseAPI.hasher.EncodeInt64([]int64{int64(purchaseID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &purchase.ListPurchasesResponse{
		NextPageToken: token,
		Purchases:     purchaseSrvPBs,
	}, nil
}

func (purchaseAPI *purchaseAPIServer) PurchaseRate(
	ctx context.Context, req *purchase.PurchaseRateRequest,
) (*purchase.PurchaseRateResponse, error) {
	payload, errr := purchaseAPI.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}

	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	}

	// if no time filter is set, lets set it from beginning of time to current moment
	if len(req.RateFilters) == 0 {
		req.RateFilters = []*purchase.PurchaseRateFilter{
			{StartTimestamp: 1, EndTimestamp: time.Now().Unix()},
		}
	}

	// Get item types as string
	its := make([]string, 0, len(req.GetItemTypes()))
	for _, v := range req.ItemTypes {
		its = append(its, v.String())
	}

	// We use Go's concurrency to speed up performance (send queries in parallel and merge results)
	var (
		resultsChan = make(chan *purchase.PurchaseRate, len(req.GetRateFilters()))
	)

	for _, rateFilter := range req.GetRateFilters() {
		rateFilter := rateFilter // alternative will be passing rateFilter as parameter to goroutine below

		// Runs concurrently to utilise multiple cores available
		go func() {
			purchaseRate, err := purchaseAPI.getPurchaseRangeRange(ctx, &rangePurchaseRateFilter{
				startTimestamp: rateFilter.GetStartTimestamp(),
				endTimestamp:   rateFilter.GetEndTimestamp(),
				userID:         payload.ID,
				rateID:         rateFilter.GetRateId(),
				itemTypes:      its,
			})
			// We don't fail the RPC on error; we simply return the error to user
			if err != nil {
				purchaseRate = &purchase.PurchaseRate{
					QuerySucceeded: false,
					StatusMessage:  err.Error(),
				}
			}

			// Send result to channel
			select {
			case <-ctx.Done(): // this cancels this operation if user (or network failure) cancels the request
			case resultsChan <- purchaseRate: // this actually sends the response to channel so it can be collected
			}
		}()
	}

	purchaseRates := make([]*purchase.PurchaseRate, 0)

loop:
	for range req.GetRateFilters() {
		// Collect results
		select {
		case <-ctx.Done():
			break loop
		case purchaseRate := <-resultsChan: // receive the response from concurrent functions
			purchaseRates = append(purchaseRates, purchaseRate)
		}
	}

	return &purchase.PurchaseRateResponse{
		PurchaseRates: purchaseRates,
	}, nil
}

// will facilitate room for adding more filters in future rather than passing many parameters to getPurchaseRangeRange
type rangePurchaseRateFilter struct {
	startTimestamp int64
	endTimestamp   int64
	userID         string
	rateID         string
	itemTypes      []string
}

// gets purchase rate between two timestamps and return a single purchase rate
func (purchaseAPI *purchaseAPIServer) getPurchaseRangeRange(ctx context.Context, rangeReq *rangePurchaseRateFilter) (*purchase.PurchaseRate, error) {
	db := purchaseAPI.SQLDB.Model(&Purchase{})

	// item type filter
	if len(rangeReq.itemTypes) > 0 {
		db = db.Where("item_type IN (?)", rangeReq.itemTypes)
	}

	// timestamp filter
	if rangeReq.startTimestamp < rangeReq.endTimestamp {
		db = db.Where("created_at BETWEEN ? AND ?", time.Unix(rangeReq.startTimestamp, 0), time.Unix(rangeReq.endTimestamp, 0))
	}

	// user id filter
	if rangeReq.userID != "" {
		db = db.Where("user_id=?", rangeReq.userID)
	}

	var itemCount int64

	// send query
	err := db.Count(&itemCount).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list purchases from database")
	}

	var totalAmount sql.NullFloat64

	// send query while reusing conditions
	err = db.Select("sum(item_cost) as total").Row().Scan(&totalAmount)
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to scan purchases list")
	}

	var totalAmountF float32

	if totalAmount.Valid {
		totalAmountF = float32(totalAmount.Float64)
	}

	return &purchase.PurchaseRate{
		RateId:         rangeReq.rateID,
		ItemCount:      int32(itemCount),
		ItemCost:       totalAmountF,
		QuerySucceeded: true,
	}, nil
}
