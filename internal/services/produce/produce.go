package produce

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"github.com/jaycynth/farm-joint/pkg/api/payment"
	"github.com/jaycynth/farm-joint/pkg/api/produce"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/api/sale"
	"github.com/jaycynth/farm-joint/pkg/option"

	"github.com/jaycynth/farm-joint/internal/services/hasher"

	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type produceAPIServer struct {
	produce.UnimplementedProduceAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewProduceAPI singleton
func NewProduceAPI(opt *option.Options) (produce.ProduceAPIServer, error) {
	//validation
	switch {
	case opt.SQLDB == nil:
		return nil, errors.New("nil db orm not allowed")
	case opt.Logger == nil:
		return nil, errors.New("nil logger not allowed")
	case opt.APIHashKey == "":
		return nil, errors.New("hashkey missing")
	case opt.PurchaseClient == nil:
		return nil, errors.New("purchase client is missing")
	case opt.PaymentClient == nil:
		return nil, errors.New("payment client is missing")
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &produceAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	if !api.SQLDB.Migrator().HasTable(produceTable) {
		err = api.SQLDB.Migrator().AutoMigrate(&Produce{})
		if err != nil {
			return nil, err
		}
	}

	return api, nil
}

func validateProduce(prd *produce.Produce) error {
	var err error
	switch {
	case prd == nil:
		err = status.Error(codes.InvalidArgument, "produce is empty")
	case prd.Date == "":
		err = status.Error(codes.InvalidArgument, "missing date")
	case prd.ProduceType == "":
		err = status.Error(codes.InvalidArgument, "missing produce type")
	case prd.Quantity == 0:
		err = status.Error(codes.InvalidArgument, "Missing quantity")
	case prd.EachPrice == 0:
		err = status.Error(codes.InvalidArgument, "Missing each price")
	case prd.FarmanimalId == "":
		err = status.Error(codes.InvalidArgument, "Missing farm animal id")
	case prd.QuantityType == produce.QuantityType_QUANTITY_TYPE_UNSPECIFIED:
		err = status.Error(codes.InvalidArgument, "Missing quantity type")
	case prd.UserId == "":
		err = status.Error(codes.InvalidArgument, "Missing user id")

	}
	return err
}

func (api *produceAPIServer) CreateProduce(ctx context.Context, createReq *produce.CreateProduceRequest) (*produce.CreateProduceResponse, error) {

	//Validation
	switch {
	case createReq == nil:
		return nil, status.Error(codes.InvalidArgument, "Empty create produce request")
	default:
		err := validateProduce(createReq.Produce)
		if err != nil {
			return nil, err
		}
	}

	//Get the produce model
	produceDB, err := GetProduceDB(createReq.Produce)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for produce from message")
	}

	// Save the object to database
	err = api.SQLDB.Create(produceDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to save produce data in db")
	}

	createReq.Produce.ProduceId = fmt.Sprint(produceDB.ProduceID)

	//convert to get an udated result for the total amount
	res, err := GetProducePB(produceDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get updated message")
	}

	return &produce.CreateProduceResponse{
		Produce:        res,
		SuccessMessage: "Successfully created a produce",
	}, nil
}

//Get produce
func (api *produceAPIServer) GetProduce(ctx context.Context, getReq *produce.GetProduceRequest) (*produce.Produce, error) {
	var produceID uint
	switch {
	case getReq == nil:
		return nil, status.Error(codes.InvalidArgument, "Empty get produce request")
	case getReq.ProduceId == "":
		return nil, status.Error(codes.InvalidArgument, "Missing Produce Id")
	default:
		id, err := strconv.Atoi(getReq.ProduceId) // ASCI to Integer
		if err != nil {
			// adc || 456.89
			return nil, status.Error(codes.InvalidArgument, "Incorrect Produce Id")
		}
		produceID = uint(id)
	}

	produceDB := &Produce{}

	err := api.SQLDB.First(produceDB, "produce_id=?", produceID).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "produce Produce does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to get produce")
	}

	// Get carProduce protobuf message
	producePB, err := GetProducePB(produceDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert produce to message")
	}

	return producePB, nil
}

const defaultPageSize = 50

//List all Produce
func (api *produceAPIServer) ListProduce(ctx context.Context, listReq *produce.ListProduceRequest) (*produce.ListProduceResponse, error) {

	payload, err := api.AuthAPI.GetPayload(ctx)
	if err != nil {
		return nil, err
	}

	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request")

	}

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var produceID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		produceID = uint(ids[0]) //95
	}

	produceSrvDBs := make([]*Produce, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("produce_id DESC")

	if produceID != 0 {
		db = db.Where("produce_id<?", produceID) // Key pagination
		api.Logger.Infoln("without farm animal called")
	}

	if listReq.FarmanimalId != "" {
		db = db.Where("farm_animal_id=?", listReq.FarmanimalId)
		api.Logger.Infoln("with farm animal called")
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	// Querying for many resources
	err = db.Find(&produceSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list produce resources from database")
	}

	produceSrvPBs := make([]*produce.Produce, 0, len(produceSrvDBs))

	for _, produceSrvDB := range produceSrvDBs {
		produceSrvPB, err := GetProducePB(produceSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		produceSrvPBs = append(produceSrvPBs, produceSrvPB)
		produceID = produceSrvDB.ProduceID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(produceSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(produceID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &produce.ListProduceResponse{
		NextPageToken: token,
		ProduceList:   produceSrvPBs,
	}, nil
}

func (api *produceAPIServer) BuyProduce(ctx context.Context, buyReq *produce.BuyProduceRequest) (*produce.BuyProduceResponse, error) {
	// validate
	switch {
	case buyReq == nil:
		return nil, status.Errorf(codes.InvalidArgument, "buy req is nil")
	default:
		err := validateProduce(buyReq.Produce)
		if err != nil {
			return nil, err
		}
	}

	// Send the purchase to purchases Produce
	res, err := api.PurchaseClient.CreatePurchase(ctx, &purchase.CreatePurchaseRequest{
		Purchase: &purchase.Purchase{
			ItemId:    buyReq.Produce.ProduceId,
			ItemCost:  buyReq.Produce.EachPrice * float32(buyReq.Produce.Quantity),
			ItemType:  purchase.ItemType_PRODUCE,
			PaymentId: buyReq.GetPayment().GetPaymentId(),
		},
	})
	if err != nil {
		return nil, err
	}

	return &produce.BuyProduceResponse{
		Status: "success",
		SuccessMessage: fmt.Sprintf(
			"purchase %s for produce id %s was successful",
			res.PurchaseId, buyReq.Produce.ProduceId,
		),
		Produce: buyReq.Produce,
	}, nil
}

func (api *produceAPIServer) SellProduce(ctx context.Context, saleReq *produce.SaleProduceRequest) (*produce.SaleProduceResponse, error) {
	switch {
	case saleReq == nil:
		return nil, status.Errorf(codes.InvalidArgument, "buy req is nil")
	default:
		err := validateProduce(saleReq.Produce)
		if err != nil {
			return nil, err
		}
	}

	// // Verify that payment was made
	paymentRes, err := api.PaymentClient.ValidatePayment(ctx, &payment.ValidatePaymentRequest{
		PaymentId: saleReq.GetPayment().PaymentId,
	})
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Your payment is invalid")
	}

	var res *sale.CreateSaleResponse

	if paymentRes.IsValid {
		// Send the sale service to sales Produce
		res, err = api.SaleClient.CreateSale(ctx, &sale.CreateSaleRequest{
			Sale: &sale.Sale{
				ItemId:    saleReq.Produce.ProduceId,
				ItemType:  sale.ItemType_PRODUCE,
				PaymentId: saleReq.Payment.PaymentId,
			},
		})
		if err != nil {
			api.Logger.Error(err)
			return nil, status.Errorf(codes.InvalidArgument, "Could not create sale request")
		}
	}

	return &produce.SaleProduceResponse{
		Status: "Success",
		SuccessMessage: fmt.Sprintf(
			"sale %s for produce id %s was successful",
			res.Sale.SaleId, saleReq.Produce.ProduceId,
		),
		Produce: saleReq.Produce,
	}, nil
}
