package produce

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/produce"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const produceTable = "produce"

// Produce is a model for produce table
type Produce struct {
	ProduceID    uint `gorm:"primaryKey;autoIncrement"`
	Date         string
	ProduceType  string
	Quantity     int32
	EachPrice    float32
	TotalAmount  float32
	FarmAnimalID string
	QuantityType string
	UserID       string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
}

// TableName ...
func (*Produce) TableName() string {
	return produceTable
}

// GetProducePB - generates protocol buffers from the model class
func GetProducePB(produceDB *Produce) (*produce.Produce, error) {
	if produceDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "nil produce record")
	}

	ProducePB := &produce.Produce{
		ProduceId:    fmt.Sprint(produceDB.ProduceID),
		Date:         produceDB.Date,
		ProduceType:  produceDB.ProduceType,
		TotalPrice:   produceDB.EachPrice * float32(produceDB.Quantity),
		Quantity:     produceDB.Quantity,
		EachPrice:    produceDB.EachPrice,
		FarmanimalId: produceDB.FarmAnimalID,
		QuantityType: produce.QuantityType(produce.QuantityType_value[produceDB.QuantityType]),
		UserId:       produceDB.UserID,
	}

	return ProducePB, nil
}

//GetProduceDB - generates table from protocol buffers
func GetProduceDB(producePB *produce.Produce) (*Produce, error) {
	if producePB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "nil produce message")
	}

	produceDB := &Produce{
		Date:         producePB.GetDate(),
		ProduceType:  producePB.GetProduceType(),
		Quantity:     producePB.GetQuantity(),
		EachPrice:    producePB.GetEachPrice(),
		TotalAmount:  producePB.EachPrice * float32(producePB.Quantity),
		FarmAnimalID: producePB.GetFarmanimalId(),
		QuantityType: producePB.GetQuantityType().String(),
		UserID:       producePB.GetUserId(),
	}

	return produceDB, nil
}
