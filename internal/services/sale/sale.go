package sale

import (
	"context"
	"errors"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/sale"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type saleAPIServer struct {
	sale.UnimplementedSaleAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewSaleAPI singleton factory for sale
func NewSaleAPI(ctx context.Context, opt *option.Options) (sale.SaleAPIServer, error) {
	switch {
	case ctx == nil:
		return nil, errors.New("context must not be nil")
	case opt == nil:
		return nil, errors.New("options is required")
	case opt.SQLDB == nil:
		return nil, errors.New("sql database is required")
	case opt.RedisDB == nil:
		return nil, errors.New("redis database is required")
	case opt.Logger == nil:
		return nil, errors.New("logger should not be nil")
	case opt.APIHashKey == "":
		return nil, errors.New("missing api hash key")
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &saleAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	if !api.SQLDB.Migrator().HasTable(saleTable) {
		err = api.SQLDB.Migrator().AutoMigrate(&Sale{})
		if err != nil {
			return nil, err
		}
	}

	return api, nil
}

func (api saleAPIServer) CreateSale(ctx context.Context, req *sale.CreateSaleRequest) (*sale.CreateSaleResponse, error) {
	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	case req.Sale == nil:
		return nil, status.Error(codes.InvalidArgument, "missing sale")
	case req.Sale.ItemId == "":
		return nil, status.Error(codes.InvalidArgument, "missing item id")
	case req.Sale.ItemType == sale.ItemType_ITEM_TYPE_UNSPECIFIED:
		return nil, status.Error(codes.InvalidArgument, "missing item type")
	}

	//send to payment to  create payment sale

	// Check there error during conversion
	saleDB, err := GetSaleDB(req.Sale)
	if err != nil {
		return nil, err
	}

	// Save to db
	err = api.SQLDB.Create(saleDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to save sale")
	}

	return &sale.CreateSaleResponse{
		Sale:           req.Sale,
		SuccessMessage: "Sale for made successfully",
	}, nil
}

func (api saleAPIServer) GetSale(ctx context.Context, req *sale.GetSaleRequest) (*sale.Sale, error) {
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	case req.SaleId == "":
		return nil, status.Error(codes.InvalidArgument, "sale id")
	}

	saleDB := &Sale{}

	// Get from database
	err := api.SQLDB.First(saleDB, "sale_id=?", req.SaleId).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "sale does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "getting sale failed")
	}

	return GetSalePB(saleDB)
}

const defaultPageSize = 50

func (api saleAPIServer) ListSales(ctx context.Context, listReq *sale.ListSalesRequest) (*sale.ListSalesResponse, error) {
	payload, errr := api.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}

	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request")
	}
	var err error

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var saleID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		saleID = uint(ids[0]) //95
	}

	saleSrvDBs := make([]*Sale, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("sale_id DESC")

	if pageToken != "" {
		db = db.Where("sale_id<?", saleID) // Key pagination
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	// Querying for many resources
	err = db.Find(&saleSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to get sales list from database")
	}

	saleSrvPBs := make([]*sale.Sale, 0, len(saleSrvDBs)) // 1 allocation

	for _, saleSrvDB := range saleSrvDBs {
		saleSrvPB, err := GetSalePB(saleSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		saleSrvPBs = append(saleSrvPBs, saleSrvPB)
		saleID = saleSrvDB.SaleID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(saleSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(saleID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &sale.ListSalesResponse{
		NextPageToken: token,
		SaleList:      saleSrvPBs,
	}, nil
}
