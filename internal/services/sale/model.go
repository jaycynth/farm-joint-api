package sale

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/sale"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const saleTable = "sales"

// Sale model
type Sale struct {
	SaleID    uint `gorm:"primaryKey;autoIncrement"`
	ItemID    string
	ItemType  string
	PaymentID string
	UserID    string
	CreatedAt time.Time
}

// GetSaleDB ..
func GetSaleDB(salePB *sale.Sale) (*Sale, error) {
	if salePB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "sale pb message is empty")
	}

	saleDB := &Sale{
		ItemID:    salePB.GetItemId(),
		ItemType:  salePB.GetItemType().String(),
		PaymentID: salePB.GetItemId(),
		UserID:    salePB.GetUserId(),
	}
	return saleDB, nil
}

// GetSalePB ..
func GetSalePB(saleDB *Sale) (*sale.Sale, error) {
	if saleDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "sale model is empty")
	}
	salePB := &sale.Sale{
		SaleId:   fmt.Sprint(saleDB.SaleID),
		ItemId:   saleDB.ItemID,
		ItemType: sale.ItemType(sale.ItemType_value[saleDB.ItemType]),
		UserId:   saleDB.UserID,
	}
	return salePB, nil
}

// TableName ..
func TableName(*Sale) string {
	return saleTable
}
