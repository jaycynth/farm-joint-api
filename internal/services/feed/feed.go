package feed

import (
	"context"
	"errors"
	"fmt"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/feed"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type feedAPIServer struct {
	feed.UnimplementedFeedAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewFeedAPI ...
func NewFeedAPI(ctx context.Context, opt *option.Options) (feed.FeedAPIServer, error) {
	//validate
	switch {
	case ctx == nil:
		return nil, errors.New("ctx is nil")
	case opt.APIHashKey == "":
		return nil, errors.New("hash key is empty")
	case opt.SQLDB == nil:
		return nil, errors.New("SQLDB is nil")
	case opt.Logger == nil:
		return nil, errors.New("logger should not be nil")
	case opt.PurchaseClient == nil:
		return nil, errors.New("purchase client is missing")
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &feedAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	// if !api.SQLDB.Migrator().HasTable(feedTable) {
	err = api.SQLDB.Migrator().AutoMigrate(&Feed{})
	if err != nil {
		return nil, err
	}
	// }

	return api, nil
}

func validateFeed(fd *feed.Feed) error {
	var err error
	switch {
	case fd.GetDate() == "":
		err = status.Errorf(codes.InvalidArgument, "date is empty")
	case fd.GetPrice() == "":
		err = status.Errorf(codes.InvalidArgument, "price is empty")
	case fd.GetQuantity() == "":
		err = status.Errorf(codes.InvalidArgument, "quantity is empty")
	case fd.GetName() == "":
		err = status.Errorf(codes.InvalidArgument, "feed name is empty")
	}
	return err
}

func (api *feedAPIServer) CreateFeed(ctx context.Context, req *feed.CreateFeedRequest) (*feed.Feed, error) {
	switch {
	case req == nil:
		return nil, status.Errorf(codes.InvalidArgument, "req is nil")
	default:
		err := validateFeed(req.Feed)
		if err != nil {
			return nil, err
		}
	}

	feedDB, err := GetFeedDB(req.Feed)
	if err != nil {
		return nil, err
	}

	// Save to db
	err = api.SQLDB.Create(feedDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to save feed")
	}

	return GetFeedPB(feedDB)
}
func (api *feedAPIServer) GetFeed(ctx context.Context, req *feed.GetFeedRequest) (*feed.Feed, error) {
	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	case req.FeedId == "":
		return nil, status.Error(codes.InvalidArgument, "feed id")
	}

	feedDB := &Feed{}

	// Get from database
	err := api.SQLDB.First(feedDB, "feed_id=?", req.FeedId).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "feed does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "getting feed failed")
	}

	return GetFeedPB(feedDB)
}

const defaultPageSize = 50

func (api *feedAPIServer) ListFeed(ctx context.Context, req *feed.ListFeedRequest) (*feed.ListFeedResponse, error) {

	payload, errr := api.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}

	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	}

	var err error

	pageSize := req.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var feedID uint
	pageToken := req.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(req.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		feedID = uint(ids[0]) //95
	}

	feedSrvDBs := make([]*Feed, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("feed_id DESC")

	if pageToken != "" {
		db = db.Where("feed_id<?", feedID) // Key pagination
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	// Querying for many resources
	err = db.Find(&feedSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list feed from database")
	}

	feedSrvPBs := make([]*feed.Feed, 0, len(feedSrvDBs)) // 1 allocation

	for _, feedSrvDB := range feedSrvDBs {
		feedSrvPB, err := GetFeedPB(feedSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		feedSrvPBs = append(feedSrvPBs, feedSrvPB)
		feedID = feedSrvDB.FeedID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(feedSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(feedID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &feed.ListFeedResponse{
		NextPageToken: token,
		FeedList:      feedSrvPBs,
	}, nil
}
func (api *feedAPIServer) BuyFeed(ctx context.Context, buyReq *feed.BuyFeedRequest) (*feed.BuyFeedResponse, error) {
	// validate
	switch {
	case buyReq == nil:
		return nil, status.Errorf(codes.InvalidArgument, "buy req is nil")
	default:
		err := validateFeed(buyReq.Feed)
		if err != nil {
			return nil, err
		}
	}

	// Send the purchase to purchases service
	res, err := api.PurchaseClient.CreatePurchase(ctx, &purchase.CreatePurchaseRequest{
		Purchase: &purchase.Purchase{
			ItemId:    buyReq.Feed.FeedId,
			ItemType:  purchase.ItemType_FEED,
			PaymentId: buyReq.Payment.PaymentId,
		},
	})
	if err != nil {
		return nil, err
	}

	return &feed.BuyFeedResponse{
		Status: "success",
		SuccessMessage: fmt.Sprintf(
			"purchase %s for produce id %s was successful",
			res.PurchaseId, buyReq.Feed.FeedId,
		),
		Feed: buyReq.Feed,
	}, nil

}
