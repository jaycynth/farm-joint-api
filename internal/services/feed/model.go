package feed

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/feed"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const feedTable = "feeds"

//Feed ...
type Feed struct {
	FeedID    uint `gorm:"primaryKey;autoIncrement"`
	Date      string
	Quantity  string
	Price     string
	UserID    string
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

// GetFeedPB ..
func GetFeedPB(feedDB *Feed) (*feed.Feed, error) {
	if feedDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, " feed db is nil")
	}
	feedPB := &feed.Feed{
		FeedId:   fmt.Sprint(feedDB.FeedID),
		Date:     feedDB.Date,
		Quantity: feedDB.Quantity,
		Price:    feedDB.Price,
		Name:     feedDB.Name,
		UserId:   feedDB.UserID,
	}

	return feedPB, nil
}

// GetFeedDB ..
func GetFeedDB(feedPB *feed.Feed) (*Feed, error) {
	if feedPB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "feed pb is nil")
	}
	feedDB := &Feed{
		Date:     feedPB.GetDate(),
		Quantity: feedPB.GetQuantity(),
		Price:    feedPB.GetPrice(),
		UserID:   feedPB.GetUserId(),
		Name:     feedPB.GetName(),
	}
	return feedDB, nil
}

// TableName ...
func (*Feed) TableName() string {
	return feedTable
}
