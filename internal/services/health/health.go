package health

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/health"
	"github.com/jaycynth/farm-joint/pkg/option"
)

type healthAPIServer struct {
	health.UnimplementedHealthAPIServer
	*option.Options
	hasher *hashids.HashID
}

//NewHealthAPI singleton
func NewHealthAPI(opt *option.Options) (health.HealthAPIServer, error) {
	//validation
	switch {
	case opt.SQLDB == nil:
		return nil, status.Error(codes.InvalidArgument, "nil db orm not allowed")
	case opt.Logger == nil:
		return nil, status.Error(codes.InvalidArgument, "nil logger not allowed")
	case opt.APIHashKey == "":
		return nil, status.Error(codes.InvalidArgument, "hashkey missing")
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &healthAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	if !api.SQLDB.Migrator().HasTable(healthTable) {
		err = api.SQLDB.Migrator().AutoMigrate(&Health{})
		if err != nil {
			return nil, err
		}
	}

	return api, nil

}

func (api *healthAPIServer) CreateHealth(ctx context.Context, createReq *health.CreateHealthRequest) (*health.Health, error) {
	//Validation
	switch {
	case createReq == nil:
		return nil, status.Errorf(codes.InvalidArgument, "request is nil")
	case createReq.GetHealth().GetDate() == "":
		return nil, status.Errorf(codes.InvalidArgument, "Date is empty")
	case createReq.GetHealth().GetDescription() == "":
		return nil, status.Errorf(codes.InvalidArgument, "Description is empty")
	case createReq.GetHealth().GetProcedureType() == "":
		return nil, status.Errorf(codes.InvalidArgument, "procedure type is  empty")
	case createReq.GetHealth().GetVetName() == "":
		return nil, status.Errorf(codes.InvalidArgument, " vet name is empty")
	case createReq.GetHealth().GetFarmanimalId() == "":
		return nil, status.Errorf(codes.InvalidArgument, "farm animal id is missing")
	}

	//Get the health model
	healthDB, err := GetHealthDB(createReq.Health)

	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for health")
	}

	// Save the object to database
	err = api.SQLDB.Create(healthDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed save health")
	}

	createReq.Health.HealthId = fmt.Sprint(healthDB.HealthID)

	return createReq.Health, nil
}

// GetHealth Method
func (api *healthAPIServer) GetHealth(ctx context.Context, getReq *health.GetHealthRequest) (*health.Health, error) {
	var healthID uint
	switch {
	case getReq == nil:
		return nil, status.Error(codes.InvalidArgument, "empty request")
	case getReq.HealthId == "":
		return nil, status.Error(codes.InvalidArgument, "missing request id")
	default:
		id, err := strconv.Atoi(getReq.HealthId) // ASCI to Integer
		if err != nil {
			// adc || 456.89
			return nil, status.Error(codes.InvalidArgument, "incorrect request id")
		}
		healthID = uint(id)
	}

	healthDB := &Health{}

	err := api.SQLDB.First(healthDB, "request_id=?", healthID).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "health service does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to get health")
	}

	healthPB, err := GetHealthPB(healthDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert health to message")
	}

	return healthPB, nil
}

const defaultPageSize = 50

func (api *healthAPIServer) ListHealth(ctx context.Context, listReq *health.ListHealthRequest) (*health.ListHealthResponse, error) {

	payload, errr := api.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}

	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request")
	}
	var err error

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var healthID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		healthID = uint(ids[0]) //95
	}

	healthSrvDBs := make([]*Health, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("health_id DESC")

	if pageToken != "" {
		db = db.Where("health_id<?", healthID) // Key pagination
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	// Querying for many resources
	err = db.Find(&healthSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "faield to list resources from database")
	}

	healthSrvPBs := make([]*health.Health, 0, len(healthSrvDBs)) // 1 allocation

	for _, healthSrvDB := range healthSrvDBs {
		healthSrvPB, err := GetHealthPB(healthSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		healthSrvPBs = append(healthSrvPBs, healthSrvPB)
		healthID = healthSrvDB.HealthID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(healthSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(healthID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &health.ListHealthResponse{
		NextPageToken: token,
		HealthList:    healthSrvPBs,
	}, nil
}
