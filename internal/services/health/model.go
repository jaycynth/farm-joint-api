package health

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/health"
)

const healthTable = "health"

//Health  Model
type Health struct {
	HealthID      uint `gorm:"primaryKey;autoIncrement"`
	Date          string
	ProcedureType string
	Description   string
	VetName       string
	FarmAnimalID  string
	UserID        string
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}

// GetHealthPB method
func GetHealthPB(healthDB *Health) (*health.Health, error) {
	servicePB := &health.Health{
		HealthId:      fmt.Sprint(healthDB.HealthID),
		Date:          healthDB.Date,
		ProcedureType: healthDB.ProcedureType,
		Description:   healthDB.Description,
		VetName:       healthDB.VetName,
		FarmanimalId:  healthDB.FarmAnimalID,
		UserId:        healthDB.UserID,
	}

	return servicePB, nil
}

// GetHealthDB from protocol buffer message
func GetHealthDB(healthPB *health.Health) (*Health, error) {
	healthDB := &Health{
		Date:          healthPB.GetDate(),
		ProcedureType: healthPB.GetProcedureType(),
		Description:   healthPB.GetDescription(),
		VetName:       healthPB.GetVetName(),
		FarmAnimalID:  healthPB.GetFarmanimalId(),
		UserID:        healthPB.GetUserId(),
	}

	return healthDB, nil
}

// TableName ...
func TableName(*Health) string {
	return healthTable
}
