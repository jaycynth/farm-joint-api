package supplement

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/supplement"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const supplementTable = "supplements"

// Supplement ..
type Supplement struct {
	SupplementID   uint `gorm:"primaryKey;autoIncrement"`
	SupplementName string
	Units          string
	Quantity       string
	Price          string
	DateBought     string
	UserID         string
	CreatedAt      time.Time
	UpdatedAt      time.Time
}

// GetSupplementPB ..
func GetSupplementPB(supplementDB *Supplement) (*supplement.Supplement, error) {
	if supplementDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "nil supplement record")
	}
	supplementPB := &supplement.Supplement{
		SupplementId:   fmt.Sprint(supplementDB.SupplementID),
		SupplementName: supplementDB.SupplementName,
		Units:          supplementDB.Units,
		Quantity:       supplementDB.Quantity,
		Price:          supplementDB.Price,
		DateBought:     supplementDB.DateBought,
		UserId:         supplementDB.UserID,
	}

	return supplementPB, nil
}

// GetSupplementDB ..
func GetSupplementDB(supplementPB *supplement.Supplement) (*Supplement, error) {
	if supplementPB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "nil supplement message")
	}
	supplementDB := &Supplement{
		SupplementName: supplementPB.GetSupplementName(),
		Units:          supplementPB.GetUnits(),
		Quantity:       supplementPB.GetQuantity(),
		Price:          supplementPB.GetPrice(),
		DateBought:     supplementPB.GetDateBought(),
		UserID:         supplementPB.GetUserId(),
	}

	return supplementDB, nil
}

// TableName ...
func (*Supplement) TableName() string {
	return supplementTable
}
