package supplement

import (
	"context"
	"errors"
	"fmt"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/api/supplement"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type supplementAPIServer struct {
	supplement.UnimplementedSupplementAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewSupplementAPI ; singleton
func NewSupplementAPI(ctx context.Context, opt *option.Options) (supplement.SupplementAPIServer, error) {
	switch {
	case ctx == nil:
		return nil, errors.New("ctx is nil")
	case opt.APIHashKey == "":
		return nil, errors.New("hash key is empty")
	case opt.SQLDB == nil:
		return nil, errors.New("SQLDB is nil")
	case opt.Logger == nil:
		return nil, errors.New("logger should not be nil")
	case opt.PurchaseClient == nil:
		return nil, errors.New("purchase client is missing")
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &supplementAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	// if !api.SQLDB.Migrator().HasTable(supplementTable) {
	err = api.SQLDB.Migrator().AutoMigrate(&Supplement{})
	if err != nil {
		return nil, err
	}
	// }

	return api, nil
}

func validateSupplement(supp *supplement.Supplement) error {
	var err error
	switch {
	case supp.SupplementName == "":
		err = status.Errorf(codes.InvalidArgument, "date is empty")
	case supp.Units == "":
		err = status.Errorf(codes.InvalidArgument, "price is empty")
	case supp.Quantity == "":
		err = status.Errorf(codes.InvalidArgument, "quantity is empty")
	case supp.Price == "":
		err = status.Errorf(codes.InvalidArgument, "price is empty")
	case supp.DateBought == "":
		err = status.Errorf(codes.InvalidArgument, "date bought is empty")
	}
	return err
}

func (api supplementAPIServer) CreateSupplement(ctx context.Context, req *supplement.CreateSupplementRequest) (*supplement.Supplement, error) {

	switch {
	case req == nil:
		return nil, status.Errorf(codes.InvalidArgument, "req is nil")
	default:
		err := validateSupplement(req.Supplement)
		if err != nil {
			return nil, err
		}
	}

	supplementDB, err := GetSupplementDB(req.Supplement)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for supplement from message")
	}

	// Save to db
	err = api.SQLDB.Create(supplementDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to save supplement")
	}

	supplementPB, err := GetSupplementPB(supplementDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert supplement to message")
	}

	return supplementPB, nil
}

func (api supplementAPIServer) GetSupplement(ctx context.Context, req *supplement.GetSupplementRequest) (*supplement.Supplement, error) {
	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	case req.SupplementId == "":
		return nil, status.Error(codes.InvalidArgument, "supplement id is missing")
	}

	supplementDB := &Supplement{}

	// Get from database
	err := api.SQLDB.First(supplementDB, "supplement_id=?", req.SupplementId).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "supplement does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "getting supplement failed")
	}

	return GetSupplementPB(supplementDB)
}

const defaultPageSize = 50

func (api supplementAPIServer) ListSupplement(ctx context.Context, req *supplement.ListSupplementRequest) (*supplement.ListSupplementResponse, error) {

	payload, errr := api.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}

	// Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request not allowed")
	}

	var err error

	pageSize := req.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var supplementID uint
	pageToken := req.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(req.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		supplementID = uint(ids[0]) //95
	}

	supplementSrvDBs := make([]*Supplement, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("supplement_id DESC")

	if pageToken != "" {
		db = db.Where("supplement_id<?", supplementID) // Key pagination
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	// Querying for many resources
	err = db.Find(&supplementSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list feed from database")
	}

	supplementSrvPBs := make([]*supplement.Supplement, 0, len(supplementSrvDBs)) // 1 allocation

	for _, supplementSrvDB := range supplementSrvDBs {
		supplementSrvPB, err := GetSupplementPB(supplementSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		supplementSrvPBs = append(supplementSrvPBs, supplementSrvPB)
		supplementID = supplementSrvDB.SupplementID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(supplementSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(supplementID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &supplement.ListSupplementResponse{
		NextPageToken:  token,
		SupplementList: supplementSrvPBs,
	}, nil
}
func (api supplementAPIServer) BuySupplement(ctx context.Context, buyReq *supplement.BuySupplementRequest) (*supplement.BuySupplementResponse, error) {
	// validate
	switch {
	case buyReq == nil:
		return nil, status.Errorf(codes.InvalidArgument, "buy req is nil")
	default:
		err := validateSupplement(buyReq.Supplement)
		if err != nil {
			return nil, err
		}
	}

	// Send the purchase to purchases service
	res, err := api.PurchaseClient.CreatePurchase(ctx, &purchase.CreatePurchaseRequest{
		Purchase: &purchase.Purchase{
			ItemId:    buyReq.Supplement.SupplementId,
			ItemType:  purchase.ItemType_SUPPLEMENT,
			PaymentId: buyReq.Payment.PaymentId,
		},
	})
	if err != nil {
		return nil, err
	}

	return &supplement.BuySupplementResponse{
		Status: "success",
		SuccessMessage: fmt.Sprintf(
			"purchase %s for supplement id %s was successful",
			res.PurchaseId, buyReq.Supplement.SupplementId,
		),
		Supplement: buyReq.Supplement,
	}, nil
}
