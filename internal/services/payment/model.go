package payment

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/payment"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const paymentTable = "payments"

// Payment model
type Payment struct {
	PaymentID      uint `gorm:"primaryKey;autoIncrement"`
	Transaction    string
	PaymentTimeSec int64
	Amount         float32
	PaymentMode    string
	ItemID         string
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      *time.Time
}

// GetPaymentDB ..
func GetPaymentDB(paymentPB *payment.Payment) (*Payment, error) {
	if paymentPB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "payment message is nil")
	}

	paymentDB := &Payment{
		Transaction:    paymentPB.GetTransaction(),
		PaymentTimeSec: paymentPB.GetPaymentTimeSec(),
		Amount:         paymentPB.GetAmount(),
		PaymentMode:    paymentPB.GetPaymentMode().String(),
		ItemID:         paymentPB.GetItemId(),
	}

	return paymentDB, nil
}

// GetPaymentPB ..
func GetPaymentPB(paymentDB *Payment) (*payment.Payment, error) {
	if paymentDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "payment model is nil")
	}

	paymentPB := &payment.Payment{
		PaymentId:      fmt.Sprint(paymentDB.PaymentID),
		Transaction:    paymentDB.Transaction,
		PaymentTimeSec: paymentDB.PaymentTimeSec,
		Amount:         paymentDB.Amount,
		PaymentMode:    payment.PaymentMode(payment.PaymentMode_value[paymentDB.PaymentMode]),
		ItemId:         paymentDB.ItemID,
	}

	return paymentPB, nil
}

// TableName ..
func TableName(*Payment) string {
	return paymentTable
}
