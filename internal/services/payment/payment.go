package payment

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/payment"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type paymentAPIServer struct {
	payment.UnimplementedPaymentAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewPaymentAPI singleton
func NewPaymentAPI(opt *option.Options) (payment.PaymentAPIServer, error) {
	//validation
	switch {
	case opt.SQLDB == nil:
		return nil, errors.New("nil db orm not allowed")
	case opt.Logger == nil:
		return nil, errors.New("nil logger not allowed")
	case opt.APIHashKey == "":
		return nil, errors.New("hashkey missing")
	case opt.RedisDB == nil:
		return nil, errors.New("redis is missing")

	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &paymentAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	if !api.SQLDB.Migrator().HasTable(paymentTable) {
		err = api.SQLDB.Migrator().AutoMigrate(&Payment{})
		if err != nil {
			return nil, err
		}
	}

	return api, nil
}

// Create Payment
func (api paymentAPIServer) CreatePayment(ctx context.Context, createReq *payment.CreatePaymentRequest) (*payment.Payment, error) {
	//Validation
	switch {
	case createReq == nil:
		return nil, status.Error(codes.InvalidArgument, "Empty create payment request")
	case createReq.GetPayment().GetAmount() == 0:
		return nil, status.Error(codes.InvalidArgument, "Amount value is missing")
	case createReq.GetPayment().GetTransaction() == "":
		return nil, status.Error(codes.InvalidArgument, "Transaction value is missing")
	case createReq.GetPayment().GetPaymentTimeSec() == 0:
		return nil, status.Error(codes.InvalidArgument, "Payment time is missing")
	case createReq.GetPayment().GetPaymentMode() == payment.PaymentMode_PAYMENT_MODE_UNSPECIFIED:
		return nil, status.Error(codes.InvalidArgument, "Payment mode value is missing")
	case createReq.GetPayment().GetItemId() == "":
		return nil, status.Error(codes.InvalidArgument, "Item id value is missing")

	}

	//Get the payment model
	paymentDB, err := GetPaymentDB(createReq.Payment)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for payment from message")
	}

	// Save the object to database
	err = api.SQLDB.Create(paymentDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to save payment data in db")
	}

	createReq.Payment.PaymentId = fmt.Sprint(paymentDB.PaymentID)

	//convert to get an udated result for the total amount
	res, err := GetPaymentPB(paymentDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get updated message")
	}

	return res, nil
}

// ValidatePayment - check if payment exists in the db, then return true or false
func (api paymentAPIServer) ValidatePayment(ctx context.Context, req *payment.ValidatePaymentRequest) (*payment.ValidatePaymentResponse, error) {
	var paymentID uint
	var isValid bool
	// validate req and payment id
	switch {
	case req == nil:
		return nil, status.Errorf(codes.InvalidArgument, "req is nil")
	case req.PaymentId == "":
		return nil, status.Errorf(codes.InvalidArgument, "Payment Id is missing")
	default:
		id, err := strconv.Atoi(req.PaymentId)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Incorrect Produce Id")
		}
		paymentID = uint(id)
	}

	paymentDB := &Payment{}

	err := api.SQLDB.First(paymentDB, "payment_id = ?", paymentID).Error

	switch {
	case err == nil:
		isValid = true
	case errors.Is(err, gorm.ErrRecordNotFound):
		isValid = false
		return nil, status.Error(codes.NotFound, "the payment doesn't exist in db")
	default:
		api.Logger.Errorln(err)
		isValid = false
		return nil, status.Error(codes.Internal, "failed to get payment record from db")
	}

	return &payment.ValidatePaymentResponse{
		IsValid: isValid,
	}, nil
}
