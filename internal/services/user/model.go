package user

import (
	"fmt"
	"time"

	"github.com/jaycynth/farm-joint/pkg/api/user"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// UniqueConstraintUsername ...
	UniqueConstraintUsername = "users_username_key"
	// UniqueConstraintPhoneNumber ...
	UniqueConstraintPhoneNumber = "users_phone_number_key"

	userTable = "user"
)

//User ...
type User struct {
	UserID      uint `gorm:"primaryKey;autoIncrement"`
	UserName    string
	PhoneNumber string
	Group       string `gorm:"type:varchar(20);default:'USER'"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

// GetUserPB ...
func GetUserPB(userDB *User) (*user.User, error) {
	if userDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "user db is empty")
	}
	userPB := &user.User{
		UserId:      fmt.Sprint(userDB.UserID),
		UserName:    userDB.UserName,
		PhoneNumber: userDB.PhoneNumber,
	}

	return userPB, nil
}

//GetUserDB ...
func GetUserDB(userPB *user.User) (*User, error) {
	if userPB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "user pb message is empty")
	}

	userDB := &User{
		UserName:    userPB.GetUserName(),
		PhoneNumber: userPB.GetPhoneNumber(),
	}

	return userDB, nil
}

// TableName ...
func (*User) TableName() string {
	return userTable
}

// UsernameDuplicateError ...
type UsernameDuplicateError struct {
	Username string
}

func (e *UsernameDuplicateError) Error() string {
	return fmt.Sprintf("Username '%s' already exists", e.Username)
}

// PhoneNumberDuplicateError ...
type PhoneNumberDuplicateError struct {
	PhoneNumber string
}

func (e *PhoneNumberDuplicateError) Error() string {
	return fmt.Sprintf("Phone number '%s' already exists", e.PhoneNumber)
}

// PhoneNumberNotExistsError ...
type PhoneNumberNotExistsError struct{}

func (*PhoneNumberNotExistsError) Error() string {
	return "phone number does not exist"
}
