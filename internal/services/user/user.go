package user

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/user"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type userAPIServer struct {
	user.UnimplementedUserAPIServer
	*option.Options
	hasher  *hashids.HashID
	authAPI auth.API
}

// NewUserAPI ...
func NewUserAPI(ctx context.Context, opt *option.Options) (user.UserAPIServer, error) {
	switch {
	case ctx == nil:
		return nil, errors.New("ctx is nil")
	case opt.APIHashKey == "":
		return nil, errors.New("hash key is empty")
	case opt.SQLDB == nil:
		return nil, errors.New("SQLDB is nil")
	case opt.Logger == nil:
		return nil, errors.New("logger should not be nil")

	}

	// The authentication
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "user API")
	if err != nil {
		return nil, err
	}

	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &userAPIServer{
		Options: opt,
		hasher:  hasher,
		authAPI: authAPI,
	}

	// Auto migration
	if !api.SQLDB.Migrator().HasTable(userTable) {
		err = api.SQLDB.Migrator().AutoMigrate(&User{})
		if err != nil {
			return nil, err
		}
	}

	return api, nil
}

func validateUser(usr *user.User) error {
	var err error
	switch {
	case usr == nil:
		err = status.Error(codes.InvalidArgument, "user is empty")
	case usr.UserName == "":
		err = status.Error(codes.InvalidArgument, "missing user name")
	case usr.PhoneNumber == "":
		err = status.Error(codes.InvalidArgument, "missing user phone number")

	}
	return err
}

// CreateUser: User Sign up
func (api *userAPIServer) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.User, error) {

	//Validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "Empty user request")
	default:
		err := validateUser(req.User)
		if err != nil {
			return nil, err
		}
	}

	//Get the user model
	userDB, err := GetUserDB(req.User)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for user from message")
	}

	// Force group to be user upon creation
	userDB.Group = "USER"

	// Save the object to database
	err = api.SQLDB.Create(userDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		if auth.IsUniqueConstraintError(err, UniqueConstraintUsername) {
			return nil, &UsernameDuplicateError{Username: req.User.UserName}
		}
		if auth.IsUniqueConstraintError(err, UniqueConstraintPhoneNumber) {
			return nil, &PhoneNumberDuplicateError{PhoneNumber: req.User.PhoneNumber}
		}
		//log eror with %
		return nil, status.Error(codes.Internal, "Failed to save user data in db %d")
	}

	req.User.UserId = fmt.Sprint(userDB.UserID)

	//convert to get an udated result for the total amount
	res, err := GetUserPB(userDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get updated message")
	}

	return res, nil
}

// Login : User login
func (api *userAPIServer) Login(ctx context.Context, req *user.LoginRequest) (*user.LoginResponse, error) {

	//validation
	switch {
	case req == nil:
		return nil, status.Error(codes.InvalidArgument, "Empty login request")
	case req.User.UserName == "":
		return nil, status.Error(codes.InvalidArgument, "login request missing username")
	case req.User.PhoneNumber == "":
		return nil, status.Error(codes.InvalidArgument, "login request missing phone number")
	}

	userDB := &User{}
	err := api.SQLDB.Find(userDB, "phone_number=?", req.GetUser().GetPhoneNumber()).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "user with the phone does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to get user")
	}

	// Match between request and user
	if userDB.UserName != req.GetUser().GetUserName() {
		return nil, status.Error(codes.Unauthenticated, "wrong credentials")
	}

	// Generate jwt
	token, err := api.authAPI.GenToken(
		ctx,
		&auth.Payload{
			ID:          fmt.Sprint(userDB.UserID),
			Group:       userDB.Group,
			Names:       req.User.UserName,
			PhoneNumber: req.User.PhoneNumber,
		},
		int64(365*24*time.Hour),
	)

	if err != nil {
		api.Logger.Errorln("failed to generate jwt token: ", err)
		return nil, status.Error(codes.Internal, "failed to generate token")

	}

	userPB, err := GetUserPB(userDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert user to message")
	}

	return &user.LoginResponse{
		JwtToken: token,
		User:     userPB,
	}, nil
}

// GetUser : Get a single user
func (api *userAPIServer) GetUser(ctx context.Context, getReq *user.UserRequest) (*user.User, error) {
	var userID uint
	switch {
	case getReq == nil:
		return nil, status.Error(codes.InvalidArgument, "Empty get user request")
	case getReq.UserId == "":
		return nil, status.Error(codes.InvalidArgument, "Missing User Id")
	default:
		id, err := strconv.Atoi(getReq.UserId)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Incorrect User Id")
		}
		userID = uint(id)
	}

	userDB := &User{}

	err := api.SQLDB.First(userDB, "user_id=?", userID).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "user does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to get user")
	}

	// Get protobuf message
	userPB, err := GetUserPB(userDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert user to message")
	}

	return userPB, nil
}

const defaultPageSize = 50

// LisUsers : list all the users
func (api *userAPIServer) ListUsers(ctx context.Context, listReq *user.ListUserRequest) (*user.ListUserResponse, error) {
	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "list request is nil")

	}
	var err error

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var userID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		userID = uint(ids[0])
	}

	userSrvDBs := make([]*User, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("user_id DESC")

	if pageToken != "" {
		db = db.Where("user_id<?", userID)
	}

	err = db.Find(&userSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list user resources from database")
	}

	userSrvPBs := make([]*user.User, 0, len(userSrvDBs))

	for _, userSrvDB := range userSrvDBs {
		userSrvPB, err := GetUserPB(userSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		userSrvPBs = append(userSrvPBs, userSrvPB)
		userID = userSrvDB.UserID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(userSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(userID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &user.ListUserResponse{
		NextPageToken: token,
		UserList:      userSrvPBs,
	}, nil
}

func (api *userAPIServer) UpdateUser(ctx context.Context, req *user.UserRequest) (*user.User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateUser not implemented")
}

func (api *userAPIServer) DeleteUser(ctx context.Context, req *user.UserRequest) (*user.DeleteUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteUser not implemented")
}
