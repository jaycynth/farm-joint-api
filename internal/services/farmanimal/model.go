package farmanimal

import (
	"fmt"

	"github.com/jaycynth/farm-joint/pkg/api/farmanimal"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const farmanimalTable = "farmanimal"

// FarmAnimal farm animal
type FarmAnimal struct {
	FarmAnimalID uint `gorm:"primaryKey;autoIncrement"`
	Tag          string
	Photo        string
	Status       string `gorm:"index;type:enum('HEALTHY','SICKLY','OLD_AGED','PREGNANT','EXPECTANT')"`
	BirthDate    string
	CalfingCount string
}

// TableName ...
func (*FarmAnimal) TableName() string {
	return farmanimalTable
}

// GetFarmAnimalPB -get protocol buffer message from model
func GetFarmAnimalPB(farmAnimalDB *FarmAnimal) (*farmanimal.FarmAnimal, error) {
	if farmAnimalDB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "missing farm animal model")
	}
	farmAnimalPB := &farmanimal.FarmAnimal{
		FarmanimalId: fmt.Sprint(farmAnimalDB.FarmAnimalID),
		Tag:          farmAnimalDB.Tag,
		Photo:        farmAnimalDB.Photo,
		Status:       farmanimal.Status(farmanimal.Status_value[farmAnimalDB.Status]),
		BirthDate:    farmAnimalDB.BirthDate,
		CalfingCount: farmAnimalDB.CalfingCount,
	}

	return farmAnimalPB, nil
}

// GetFarmAnimalDB - get model from protocol buffer message
func GetFarmAnimalDB(farmAnimalPB *farmanimal.FarmAnimal) (*FarmAnimal, error) {
	if farmAnimalPB == nil {
		return nil, status.Errorf(codes.InvalidArgument, "missing farm animal message")
	}
	farmAnimalDB := &FarmAnimal{
		Tag:          farmAnimalPB.GetTag(),
		Photo:        farmAnimalPB.GetPhoto(),
		Status:       farmAnimalPB.GetStatus().String(),
		BirthDate:    farmAnimalPB.GetBirthDate(),
		CalfingCount: farmAnimalPB.GetCalfingCount(),
	}

	return farmAnimalDB, nil
}
