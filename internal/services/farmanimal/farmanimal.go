package farmanimal

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"github.com/jaycynth/farm-joint/internal/services/hasher"
	"github.com/jaycynth/farm-joint/pkg/api/farmanimal"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/speps/go-hashids"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type farmAnimalAPI struct {
	farmanimal.UnimplementedFarmAnimalAPIServer
	*option.Options
	hasher *hashids.HashID
}

// NewFarmAnimalAPI - singleton for farmanimal
func NewFarmAnimalAPI(opt *option.Options) (farmanimal.FarmAnimalAPIServer, error) {
	//validate
	switch {
	case opt.SQLDB == nil:
		return nil, status.Error(codes.InvalidArgument, "nil db orm not allowed")
	case opt.Logger == nil:
		return nil, status.Error(codes.InvalidArgument, "nil logger not allowed")
	case opt.APIHashKey == "":
		return nil, status.Error(codes.InvalidArgument, "hashkey missing")
	}

	//hasher generation
	hasher, err := hasher.NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, fmt.Errorf("failed to create hasher: %v", err)
	}

	api := &farmAnimalAPI{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	// if !api.SQLDB.Migrator().HasTable(farmanimalTable) {
	err = api.SQLDB.Migrator().AutoMigrate(&FarmAnimal{})
	if err != nil {
		return nil, err
	}
	// }
	return api, nil
}

func (api *farmAnimalAPI) CreateFarmAnimal(ctx context.Context, createReq *farmanimal.CreateFarmAnimalRequest) (*farmanimal.CreateFarmAnimalResponse, error) {
	//validate request
	switch {
	case createReq == nil:
		return nil, status.Errorf(codes.InvalidArgument, "request is nil")
	case createReq.GetFarmanimal().GetTag() == "":
		return nil, status.Errorf(codes.InvalidArgument, " tag is empty")
	case createReq.GetFarmanimal().GetPhoto() == "":
		return nil, status.Errorf(codes.InvalidArgument, " photo is empty")
	case createReq.GetFarmanimal().GetStatus() == farmanimal.Status_STATUS_UNSPECIFIED:
		return nil, status.Errorf(codes.InvalidArgument, "farm animal status is empty")
	case createReq.GetFarmanimal().GetBirthDate() == "":
		return nil, status.Errorf(codes.InvalidArgument, "birth date is empty")
	case createReq.GetFarmanimal().GetCalfingCount() == "":
		return nil, status.Errorf(codes.InvalidArgument, "calfing count is empty")

	}

	//get model from protobuf message
	createAnimalDB, err := GetFarmAnimalDB(createReq.Farmanimal)

	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for farm animal")
	}

	// Save the object to database
	err = api.SQLDB.Create(createAnimalDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed save farm animal to db")
	}

	createReq.Farmanimal.FarmanimalId = fmt.Sprint(createAnimalDB.FarmAnimalID)

	//convert to get an udated result for the total amount
	res, err := GetFarmAnimalPB(createAnimalDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get updated message")
	}

	return &farmanimal.CreateFarmAnimalResponse{
		SuccessMessage: "Created Farm Animal Successfully",
		Farmanimal:     res,
	}, nil
}

func (api *farmAnimalAPI) GetFarmAnimal(ctx context.Context, getReq *farmanimal.GetFarmAnimalRequest) (*farmanimal.FarmAnimal, error) {
	var farmanimalID uint
	//validate request
	switch {
	case getReq == nil:
		return nil, status.Error(codes.InvalidArgument, "empty request")
	case getReq.FarmanimalId == "":
		return nil, status.Error(codes.InvalidArgument, "missing farm animal id")
	default:
		id, err := strconv.Atoi(getReq.FarmanimalId)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "incorrect farm animal id")
		}
		farmanimalID = uint(id)
	}

	farmAnimalDB := &FarmAnimal{}

	err := api.SQLDB.First(farmAnimalDB, "farmanimal_id=?", farmanimalID).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "farm animal record does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to get farm animal")
	}

	// Get carservice protobuf message
	farmanimalPB, err := GetFarmAnimalPB(farmAnimalDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert farm animal to message")
	}

	return farmanimalPB, nil
}

const defaultPageSize = 50

func (api *farmAnimalAPI) ListFarmAnimals(ctx context.Context, listReq *farmanimal.ListFarmAnimalsRequest) (*farmanimal.ListFarmAnimalsResponse, error) {
	payload, errr := api.AuthAPI.GetPayload(ctx)
	if errr != nil {
		return nil, errr
	}

	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "nil list request")
	}
	var err error

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var farmanimalID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		farmanimalID = uint(ids[0])
	}

	farmanimalSrvDBs := make([]*FarmAnimal, 0, pageSize)

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("farm_animal_id DESC")

	if pageToken != "" {
		db = db.Where("farm_animal_id<?", farmanimalID)
	}

	if payload.ID != "" {
		db = db.Where("user_id=?", payload.ID)
	}

	err = db.Find(&farmanimalSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "failed to list farm animal resources from database")
	}

	farmanimalSrvPBs := make([]*farmanimal.FarmAnimal, 0, len(farmanimalSrvDBs))

	for _, farmanimalSrvDB := range farmanimalSrvDBs {
		farmanimalSrvPB, err := GetFarmAnimalPB(farmanimalSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		farmanimalSrvPBs = append(farmanimalSrvPBs, farmanimalSrvPB)
		farmanimalID = farmanimalSrvDB.FarmAnimalID
	}

	// Generating next page token
	var token string
	if int(pageSize) == len(farmanimalSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(farmanimalID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &farmanimal.ListFarmAnimalsResponse{
		NextPageToken:  token,
		FarmanimalList: farmanimalSrvPBs,
	}, nil
}

func (api *farmAnimalAPI) DeleteFarmAnimal(ctx context.Context, delReq *farmanimal.DeleteFarmAnimalRequest) (*farmanimal.DeleteFarmAnimalResponse, error) {
	var farmanimalID uint

	switch {
	case delReq == nil:
		return nil, status.Error(codes.InvalidArgument, "empty request")
	case delReq.FarmanimalId == "":
		return nil, status.Error(codes.InvalidArgument, "request missing farm animal id")
	default:
		id, err := strconv.Atoi(delReq.FarmanimalId)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "incorrect farm animal id")
		}
		farmanimalID = uint(id)
	}
	farmAnimalDB := &FarmAnimal{}

	err := api.SQLDB.Delete(farmAnimalDB, "farm_animal_id=?", farmanimalID).Error
	switch {
	case err == nil:
	case errors.Is(err, gorm.ErrRecordNotFound):
		return nil, status.Error(codes.NotFound, "farm animal record by the id given does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to delete farm animal")
	}

	return &farmanimal.DeleteFarmAnimalResponse{
		SuccessMessage: "Farm animal deleted successfuly",
	}, nil
}
