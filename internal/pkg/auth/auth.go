package auth

import (
	"context"
	"errors"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// API ...
type API interface {
	GenToken(ctx context.Context, payload *Payload, expires int64) (string, error)
	GetPayload(context.Context) (*Payload, error)
	ParseToken(string) (*Payload, error)
	AuthFunc(context.Context) (context.Context, error)
}

// Authorization
type Authorization interface {
	AuthorizeGroup(context.Context) (*Payload, error) // ADMIN, USER, SUPER_ADMIN
	AuthorizeActor(context.Context) (*Payload, error) // Client => ID in token
}

// NewAPI creates an authentication API
func NewAPI(signingKey []byte, audience, issuer string) (API, error) {
	// Validation
	switch {
	case len(signingKey) == 0:
		return nil, errors.New("jwt signing key is required")
	case audience == "":
		return nil, errors.New("audience is required")
	case issuer == "":
		return nil, errors.New("issuer is required")
	}

	// create api
	v := &authAPI{
		signingMethod: jwt.SigningMethodHS256,
		signingKey:    signingKey,
		audience:      audience,
		issuer:        issuer,
	}

	return v, nil
}

type authAPI struct {
	signingMethod jwt.SigningMethod
	signingKey    []byte
	audience      string
	issuer        string
}

func (v *authAPI) GenToken(
	ctx context.Context, payload *Payload, expires int64,
) (string, error) {
	// we call the internal implementation
	return v.genToken(ctx, payload, expires)
}

func (v *authAPI) GetPayload(ctx context.Context) (*Payload, error) {
	// type assertion; comma ok
	payload, ok := ctx.Value(ctxKey("tokenInfo")).(*Payload)
	if !ok {
		return nil, status.Error(codes.Unauthenticated, "no payload found in context")
	}
	return payload, nil
}

func (v *authAPI) ParseToken(tokenString string) (_ *Payload, err error) {
	return v.parseToken(tokenString)
}

func (v *authAPI) AuthFunc(ctx context.Context) (context.Context, error) {
	return v.authenticationFunc(ctx)
}

func (v *authAPI) genToken(
	ctx context.Context, payload *Payload, expires int64,
) (tokenStr string, err error) {
	// Handling any panic is good trust me!
	defer func() {
		if err2 := recover(); err2 != nil {
			err = fmt.Errorf("%v", err2)
		}
	}()

	token := jwt.NewWithClaims(v.signingMethod, Claims{
		Payload: payload,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expires,
			Issuer:    v.issuer,
			Audience:  v.audience,
		},
	})

	// Generate the token, generate a signed jwt
	return token.SignedString(v.signingKey)
}

func (v *authAPI) parseToken(tokenString string) (_ *Payload, err error) {
	// Handling any panic is good trust me!
	defer func() {
		if err2 := recover(); err2 != nil {
			err = fmt.Errorf("%v", err2)
		}
	}()

	token, err := jwt.ParseWithClaims(
		tokenString,
		&Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return v.signingKey, nil
		},
	)
	if err != nil {
		return nil, status.Errorf(
			codes.Unauthenticated, "failed to parse token with claims: %v", err,
		)
	}

	// type assertion to get claims; comma,ok pattern
	claims, ok := token.Claims.(*Claims)
	if !ok || !token.Valid {
		return nil, status.Error(codes.Unauthenticated, "JWT is not valid")
	}

	return claims.Payload, nil
}

func userClaimFromToken(payload *Payload) string {
	return "foobar"
}

// unexported
type ctxKey string

// authenticationFunc is authentication middleware
func (v *authAPI) authenticationFunc(ctx context.Context) (context.Context, error) {
	// Extract jwt from authorization header
	token, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return nil, err
	}

	// JWT authentication
	// header (algorith that was used for encoding the jwt) hmac => sha254,
	// payload (token metadata, username, email, group),
	// signature (concatenating the base64(header) and base64(payload)
	// base64(header).base64(payload).base64(signature)
	// 45ew2fscty2d2.d3d83d28d2v8d2d2f2.28dg28gd2gd2gd

	// Mostly

	// Decode the jwt
	tokenInfo, err := v.parseToken(token)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "invalid auth token: %v", err)
	}

	// Logging
	// grpc_ctxtags.Extract(ctx).Set("auth.sub", userClaimFromToken(tokenInfo))

	// WARNING: in production define your own type to avoid context collisions
	newCtx := context.WithValue(ctx, ctxKey("tokenInfo"), tokenInfo)

	return newCtx, nil
}
