package main

import (
	"context"
	"os"
	"time"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	farmanimal_app "github.com/jaycynth/farm-joint/internal/services/farmanimal"
	"github.com/jaycynth/farm-joint/pkg/api/farmanimal"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	handleErr(err)

	farmanimalSrv, err := micro.NewService(ctx, cfg, nil)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	farmanimalSrv.AddGRPCStreamServerInterceptors(recoverySIs...)
	farmanimalSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "farm_animal API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	farmanimalSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	token, err := authAPI.GenToken(
		ctx,
		&auth.Payload{
			Group: "ADMIN",
		},
		int64(365*24*time.Hour),
	)
	if err != nil {
		farmanimalSrv.Logger().Errorln("failed to generate jwt token: ", err)
		panic(err)
	}

	farmanimalSrv.Logger().Infoln(token)

	// Stream interceptors => Streaming APIs
	// Unary interceptors => Unary RPCs

	//set base endpoint for runtime mux
	farmanimalSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	farmanimalSrv.AddEndpoint("/api/farmanimal/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      farmanimalSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	farmanimalSrv.AddEndpoint("/api/farmanimal/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      farmanimalSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	farmanimalSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	farmanimalSrv.Start(ctx, func() error {

		farmanimalAPI, err := farmanimal_app.NewFarmAnimalAPI(&option.Options{
			SQLDB:      farmanimalSrv.GormDB(),
			Logger:     farmanimalSrv.Logger(),
			APIHashKey: os.Getenv("API_HASH_KEY"),
			AuthAPI:    authAPI,
		})
		handleErr(err)

		farmanimal.RegisterFarmAnimalAPIServer(farmanimalSrv.GRPCServer(), farmanimalAPI)

		handleErr(farmanimal.RegisterFarmAnimalAPIHandler(ctx, farmanimalSrv.RuntimeMux(), farmanimalSrv.ClientConn()))

		return nil

	})
}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
