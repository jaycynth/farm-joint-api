package main

import (
	"context"
	"os"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	user_app "github.com/jaycynth/farm-joint/internal/services/user"
	"github.com/jaycynth/farm-joint/pkg/api/user"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	userSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	userSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	userSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	//set base endpoint for runtime mux
	userSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	userSrv.AddEndpoint("/api/users/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      userSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	userSrv.AddEndpoint("/api/users/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      userSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	userSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	userSrv.Start(ctx, func() error {

		userAPI, err := user_app.NewUserAPI(ctx, &option.Options{
			SQLDB:      userSrv.GormDB(),
			Logger:     userSrv.Logger(),
			APIHashKey: os.Getenv("API_HASH_KEY"),
		})
		handleErr(err)

		user.RegisterUserAPIServer(userSrv.GRPCServer(), userAPI)

		handleErr(user.RegisterUserAPIHandler(ctx, userSrv.RuntimeMux(), userSrv.ClientConn()))

		return nil

	})

}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
