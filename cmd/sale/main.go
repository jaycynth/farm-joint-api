package main

import (
	"context"
	"os"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	sale_app "github.com/jaycynth/farm-joint/internal/services/sale"
	"github.com/jaycynth/farm-joint/pkg/api/sale"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	saleSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	saleSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	saleSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "salle API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	saleSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	//set base endpoint for runtime mux
	saleSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	saleSrv.AddEndpoint("/api/sale/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      saleSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	saleSrv.AddEndpoint("/api/sale/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      saleSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	saleSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	saleSrv.Start(ctx, func() error {
		saleAPI, err := sale_app.NewSaleAPI(ctx, &option.Options{
			SQLDB:      saleSrv.GormDB(),
			Logger:     saleSrv.Logger(),
			RedisDB:    saleSrv.RedisClientByName("redis"),
			APIHashKey: os.Getenv("API_HASH_KEY"),
			AuthAPI:    authAPI,
		})
		handleErr(err)

		sale.RegisterSaleAPIServer(saleSrv.GRPCServer(), saleAPI)

		handleErr(sale.RegisterSaleAPIHandler(ctx, saleSrv.RuntimeMux(), saleSrv.ClientConn()))

		return nil

	})
}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
