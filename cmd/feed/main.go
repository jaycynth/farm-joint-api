package main

import (
	"context"
	"os"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	feed_app "github.com/jaycynth/farm-joint/internal/services/feed"
	"github.com/jaycynth/farm-joint/pkg/api/feed"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	feedSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	feedSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	feedSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "feed API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	feedSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	//set base endpoint for runtime mux
	feedSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	feedSrv.AddEndpoint("/api/feed/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      feedSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	feedSrv.AddEndpoint("/api/feed/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      feedSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	feedSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	feedSrv.Start(ctx, func() error {
		// Dial purchase service
		feedConn, err := feedSrv.DialExternalService(ctx, "purchase")
		handleErr(err)

		feedAPI, err := feed_app.NewFeedAPI(ctx, &option.Options{
			SQLDB:          feedSrv.GormDB(),
			Logger:         feedSrv.Logger(),
			APIHashKey:     os.Getenv("API_HASH_KEY"),
			PurchaseClient: purchase.NewPurchaseAPIClient(feedConn),
			AuthAPI:        authAPI,
		})
		handleErr(err)

		feed.RegisterFeedAPIServer(feedSrv.GRPCServer(), feedAPI)

		handleErr(feed.RegisterFeedAPIHandler(ctx, feedSrv.RuntimeMux(), feedSrv.ClientConn()))

		return nil

	})

}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
