package main

import (
	"context"
	"os"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	payment_app "github.com/jaycynth/farm-joint/internal/services/payment"
	"github.com/jaycynth/farm-joint/pkg/api/payment"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	paymentSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	paymentSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	paymentSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "payment API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	paymentSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	//set base endpoint for runtime mux
	paymentSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	paymentSrv.AddEndpoint("/api/payment/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      paymentSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	paymentSrv.AddEndpoint("/api/payment/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      paymentSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	paymentSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	paymentSrv.Start(ctx, func() error {

		paymentAPI, err := payment_app.NewPaymentAPI(&option.Options{
			SQLDB:      paymentSrv.GormDB(),
			Logger:     paymentSrv.Logger(),
			RedisDB:    paymentSrv.RedisClientByName("redis"),
			APIHashKey: os.Getenv("API_HASH_KEY"),
			AuthAPI:    authAPI,
		})
		handleErr(err)

		payment.RegisterPaymentAPIServer(paymentSrv.GRPCServer(), paymentAPI)

		handleErr(payment.RegisterPaymentAPIHandler(ctx, paymentSrv.RuntimeMux(), paymentSrv.ClientConn()))

		return nil

	})
}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
