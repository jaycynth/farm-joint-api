package main

import (
	"context"
	"os"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	health_app "github.com/jaycynth/farm-joint/internal/services/health"
	"github.com/jaycynth/farm-joint/pkg/api/health"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	healthSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	healthSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	healthSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "health API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	healthSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	//set base endpoint for runtime mux
	healthSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	healthSrv.AddEndpoint("/api/healthRecord/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      healthSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	healthSrv.AddEndpoint("/api/healthRecord/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      healthSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	healthSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	healthSrv.Start(ctx, func() error {
		healthAPI, err := health_app.NewHealthAPI(&option.Options{
			SQLDB:      healthSrv.GormDB(),
			Logger:     healthSrv.Logger(),
			APIHashKey: os.Getenv("API_HASH_KEY"),
			AuthAPI:    authAPI,
		})
		handleErr(err)

		health.RegisterHealthAPIServer(healthSrv.GRPCServer(), healthAPI)

		handleErr(health.RegisterHealthAPIHandler(ctx, healthSrv.RuntimeMux(), healthSrv.ClientConn()))

		return nil

	})
}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
