package main

import (
	"context"
	"os"
	"time"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	supplement_app "github.com/jaycynth/farm-joint/internal/services/supplement"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/api/supplement"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	supplementSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	supplementSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	supplementSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "supplement API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	supplementSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	token, err := authAPI.GenToken(
		ctx,
		&auth.Payload{
			Group: "ADMIN",
		},
		int64(365*24*time.Hour),
	)
	if err != nil {
		supplementSrv.Logger().Errorln("failed to generate jwt token: ", err)
		panic(err)
	}

	supplementSrv.Logger().Infoln(token)

	//set base endpoint for runtime mux
	supplementSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	supplementSrv.AddEndpoint("/api/supplement/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      supplementSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	supplementSrv.AddEndpoint("/api/supplement/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      supplementSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	supplementSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	supplementSrv.Start(ctx, func() error {
		purchaseConn, err := supplementSrv.DialExternalService(ctx, "purchase")
		handleErr(err)

		supplementAPI, err := supplement_app.NewSupplementAPI(ctx, &option.Options{
			SQLDB:          supplementSrv.GormDB(),
			Logger:         supplementSrv.Logger(),
			APIHashKey:     os.Getenv("API_HASH_KEY"),
			PurchaseClient: purchase.NewPurchaseAPIClient(purchaseConn),
			AuthAPI:        authAPI,
		})
		handleErr(err)

		supplement.RegisterSupplementAPIServer(supplementSrv.GRPCServer(), supplementAPI)

		handleErr(supplement.RegisterSupplementAPIHandler(ctx, supplementSrv.RuntimeMux(), supplementSrv.ClientConn()))

		return nil

	})
}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
