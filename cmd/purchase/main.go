package main

import (
	"context"
	"os"
	"time"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	purchase_app "github.com/jaycynth/farm-joint/internal/services/purchase"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	purchaseSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	purchaseSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	purchaseSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "purchase API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	purchaseSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	token, err := authAPI.GenToken(
		ctx,
		&auth.Payload{
			Group: "ADMIN",
		},
		int64(365*24*time.Hour),
	)
	if err != nil {
		purchaseSrv.Logger().Errorln("failed to generate jwt token: ", err)
		panic(err)
	}

	purchaseSrv.Logger().Infoln(token)

	//set base endpoint for runtime mux
	purchaseSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	purchaseSrv.AddEndpoint("/api/purchase/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      purchaseSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	purchaseSrv.AddEndpoint("/api/purchase/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      purchaseSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	purchaseSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	purchaseSrv.Start(ctx, func() error {
		purchaseAPI, err := purchase_app.NewPurchaseAPI(ctx, &option.Options{
			SQLDB:      purchaseSrv.GormDB(),
			Logger:     purchaseSrv.Logger(),
			RedisDB:    purchaseSrv.RedisClientByName("redis"),
			APIHashKey: os.Getenv("API_HASH_KEY"),
			AuthAPI:    authAPI,
		})
		handleErr(err)

		purchase.RegisterPurchaseAPIServer(purchaseSrv.GRPCServer(), purchaseAPI)

		handleErr(purchase.RegisterPurchaseAPIHandler(ctx, purchaseSrv.RuntimeMux(), purchaseSrv.ClientConn()))

		return nil

	})
}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
