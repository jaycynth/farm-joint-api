package main

import (
	"context"
	"os"
	"time"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micro/pkg/grpc/middleware"
	"github.com/gidyon/micro/pkg/healthcheck"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	produce_app "github.com/jaycynth/farm-joint/internal/services/produce"
	"github.com/jaycynth/farm-joint/pkg/api/payment"
	"github.com/jaycynth/farm-joint/pkg/api/produce"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/api/sale"
	"github.com/jaycynth/farm-joint/pkg/option"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {

	ctx := context.Background()

	cfg, err := config.New(config.FromFile)
	handleErr(err)

	produceSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	produceSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	produceSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Create the authentication API
	authAPI, err := auth.NewAPI([]byte(os.Getenv("JWT_SIGNING_KEY")), "anyone", "produce API")
	handleErr(err)

	// AddGRPCUnaryServerInterceptors adds unary interceptors to the gRPC server
	authInterceptor := grpc_auth.UnaryServerInterceptor(authAPI.AuthFunc)
	produceSrv.AddGRPCUnaryServerInterceptors(authInterceptor)

	token, err := authAPI.GenToken(
		ctx,
		&auth.Payload{
			Group: "ADMIN",
		},
		int64(365*24*time.Hour),
	)
	if err != nil {
		produceSrv.Logger().Errorln("failed to generate jwt token: ", err)
		panic(err)
	}

	produceSrv.Logger().Infoln(token)

	//set base endpoint for runtime mux
	produceSrv.SetServeMuxEndpoint("/")

	// Readiness health check
	produceSrv.AddEndpoint("/api/produce/health/ready", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      produceSrv,
		Type:         healthcheck.ProbeReadiness,
		AutoMigrator: func() error { return nil },
	}))

	// Liveness health check
	produceSrv.AddEndpoint("/api/produce/health/live", healthcheck.RegisterProbe(&healthcheck.ProbeOptions{
		Service:      produceSrv,
		Type:         healthcheck.ProbeLiveNess,
		AutoMigrator: func() error { return nil },
	}))

	produceSrv.AddServeMuxOptions(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
			UseProtoNames:   true,
		},
	}))

	produceSrv.Start(ctx, func() error {
		// Dial purchase service
		purchaseConn, err := produceSrv.DialExternalService(ctx, "purchase")
		handleErr(err)

		//Dial sales service
		saleConn, err := produceSrv.DialExternalService(ctx, "sale")
		handleErr(err)

		//Dial payment service
		paymentConn, err := produceSrv.DialExternalService(ctx, "payment")
		handleErr(err)

		produceAPI, err := produce_app.NewProduceAPI(&option.Options{
			SQLDB:          produceSrv.GormDB().Debug(),
			Logger:         produceSrv.Logger(),
			APIHashKey:     os.Getenv("API_HASH_KEY"),
			PurchaseClient: purchase.NewPurchaseAPIClient(purchaseConn),
			SaleClient:     sale.NewSaleAPIClient(saleConn),
			PaymentClient:  payment.NewPaymentAPIClient(paymentConn),
			AuthAPI:        authAPI,
		})
		handleErr(err)

		produce.RegisterProduceAPIServer(produceSrv.GRPCServer(), produceAPI)

		handleErr(produce.RegisterProduceAPIHandler(ctx, produceSrv.RuntimeMux(), produceSrv.ClientConn()))

		return nil

	})

}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
