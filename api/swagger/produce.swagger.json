{
  "swagger": "2.0",
  "info": {
    "title": "Produce Service",
    "description": "Produce Service for Farm joint app",
    "version": "v1",
    "contact": {
      "name": "Github \u003cJuliana Cynthia\u003e",
      "email": "juliekivuva@gmail.com"
    },
    "license": {
      "name": "MIT License",
      "url": "https://github.com/jaycynth/services/blob/master/LICENSE"
    }
  },
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/api/produce": {
      "get": {
        "summary": "List all produces",
        "operationId": "ProduceAPI_ListProduce",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/produceListProduceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "pageToken",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "pageSize",
            "in": "query",
            "required": false,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "farmanimalId",
            "in": "query",
            "required": false,
            "type": "string"
          }
        ],
        "tags": [
          "ProduceAPI"
        ]
      },
      "post": {
        "summary": "Creates a new produce",
        "operationId": "ProduceAPI_CreateProduce",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/produceCreateProduceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/produceCreateProduceRequest"
            }
          }
        ],
        "tags": [
          "ProduceAPI"
        ]
      }
    },
    "/api/produce/{produceId}": {
      "get": {
        "summary": "Gets a produce",
        "operationId": "ProduceAPI_GetProduce",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/produceProduce"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "produceId",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "ProduceAPI"
        ]
      }
    },
    "/api/produce:buy": {
      "post": {
        "summary": "Buy a produce",
        "operationId": "ProduceAPI_BuyProduce",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/produceBuyProduceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/produceBuyProduceRequest"
            }
          }
        ],
        "tags": [
          "ProduceAPI"
        ]
      }
    },
    "/api/produce:list": {
      "post": {
        "summary": "List all produces",
        "operationId": "ProduceAPI_ListProduce2",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/produceListProduceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/produceListProduceRequest"
            }
          }
        ],
        "tags": [
          "ProduceAPI"
        ]
      }
    },
    "/api/produce:sale": {
      "post": {
        "summary": "sale a produce",
        "operationId": "ProduceAPI_SellProduce",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/produceSaleProduceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/produceSaleProduceRequest"
            }
          }
        ],
        "tags": [
          "ProduceAPI"
        ]
      }
    }
  },
  "definitions": {
    "paymentPayment": {
      "type": "object",
      "properties": {
        "paymentId": {
          "type": "string"
        },
        "transaction": {
          "type": "string"
        },
        "paymentTimeSec": {
          "type": "string",
          "format": "int64"
        },
        "amount": {
          "type": "number",
          "format": "float"
        },
        "paymentMode": {
          "$ref": "#/definitions/paymentPaymentMode"
        },
        "itemId": {
          "type": "string"
        },
        "userId": {
          "type": "string"
        }
      }
    },
    "paymentPaymentMode": {
      "type": "string",
      "enum": [
        "PAYMENT_MODE_UNSPECIFIED",
        "CASH",
        "MPESA",
        "CREDIT_CARD"
      ],
      "default": "PAYMENT_MODE_UNSPECIFIED"
    },
    "produceBuyProduceRequest": {
      "type": "object",
      "properties": {
        "produce": {
          "$ref": "#/definitions/produceProduce"
        },
        "payment": {
          "$ref": "#/definitions/paymentPayment"
        }
      }
    },
    "produceBuyProduceResponse": {
      "type": "object",
      "properties": {
        "status": {
          "type": "string"
        },
        "successMessage": {
          "type": "string"
        },
        "produce": {
          "$ref": "#/definitions/produceProduce"
        }
      }
    },
    "produceCreateProduceRequest": {
      "type": "object",
      "properties": {
        "produce": {
          "$ref": "#/definitions/produceProduce"
        }
      }
    },
    "produceCreateProduceResponse": {
      "type": "object",
      "properties": {
        "successMessage": {
          "type": "string"
        },
        "produce": {
          "$ref": "#/definitions/produceProduce"
        }
      }
    },
    "produceListProduceRequest": {
      "type": "object",
      "properties": {
        "pageToken": {
          "type": "string"
        },
        "pageSize": {
          "type": "integer",
          "format": "int32"
        },
        "farmanimalId": {
          "type": "string"
        }
      }
    },
    "produceListProduceResponse": {
      "type": "object",
      "properties": {
        "nextPageToken": {
          "type": "string"
        },
        "produceList": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/produceProduce"
          }
        }
      }
    },
    "produceProduce": {
      "type": "object",
      "properties": {
        "produceId": {
          "type": "string"
        },
        "date": {
          "type": "string"
        },
        "produceType": {
          "type": "string"
        },
        "quantity": {
          "type": "integer",
          "format": "int32"
        },
        "eachPrice": {
          "type": "number",
          "format": "float"
        },
        "totalPrice": {
          "type": "number",
          "format": "float"
        },
        "farmanimalId": {
          "type": "string"
        },
        "quantityType": {
          "$ref": "#/definitions/produceQuantityType"
        },
        "userId": {
          "type": "string"
        }
      }
    },
    "produceQuantityType": {
      "type": "string",
      "enum": [
        "QUANTITY_TYPE_UNSPECIFIED",
        "KG",
        "LITRE",
        "TRAY"
      ],
      "default": "QUANTITY_TYPE_UNSPECIFIED"
    },
    "produceSaleProduceRequest": {
      "type": "object",
      "properties": {
        "produce": {
          "$ref": "#/definitions/produceProduce"
        },
        "payment": {
          "$ref": "#/definitions/paymentPayment"
        }
      }
    },
    "produceSaleProduceResponse": {
      "type": "object",
      "properties": {
        "status": {
          "type": "string"
        },
        "successMessage": {
          "type": "string"
        },
        "produce": {
          "$ref": "#/definitions/produceProduce"
        }
      }
    },
    "protobufAny": {
      "type": "object",
      "properties": {
        "typeUrl": {
          "type": "string",
          "description": "A URL/resource name that uniquely identifies the type of the serialized\nprotocol buffer message. The last segment of the URL's path must represent\nthe fully qualified name of the type (as in\n`path/google.protobuf.Duration`). The name should be in a canonical form\n(e.g., leading \".\" is not accepted).\n\nIn practice, teams usually precompile into the binary all types that they\nexpect it to use in the context of Any. However, for URLs which use the\nscheme `http`, `https`, or no scheme, one can optionally set up a type\nserver that maps type URLs to message definitions as follows:\n\n* If no scheme is provided, `https` is assumed.\n* An HTTP GET on the URL must yield a [google.protobuf.Type][]\n  value in binary format, or produce an error.\n* Applications are allowed to cache lookup results based on the\n  URL, or have them precompiled into a binary to avoid any\n  lookup. Therefore, binary compatibility needs to be preserved\n  on changes to types. (Use versioned type names to manage\n  breaking changes.)\n\nNote: this functionality is not currently available in the official\nprotobuf release, and it is not used for type URLs beginning with\ntype.googleapis.com.\n\nSchemes other than `http`, `https` (or the empty scheme) might be\nused with implementation specific semantics."
        },
        "value": {
          "type": "string",
          "format": "byte",
          "description": "Must be a valid serialized protocol buffer of the above specified type."
        }
      },
      "description": "`Any` contains an arbitrary serialized protocol buffer message along with a\nURL that describes the type of the serialized message.\n\nProtobuf library provides support to pack/unpack Any values in the form\nof utility functions or additional generated methods of the Any type.\n\nExample 1: Pack and unpack a message in C++.\n\n    Foo foo = ...;\n    Any any;\n    any.PackFrom(foo);\n    ...\n    if (any.UnpackTo(\u0026foo)) {\n      ...\n    }\n\nExample 2: Pack and unpack a message in Java.\n\n    Foo foo = ...;\n    Any any = Any.pack(foo);\n    ...\n    if (any.is(Foo.class)) {\n      foo = any.unpack(Foo.class);\n    }\n\n Example 3: Pack and unpack a message in Python.\n\n    foo = Foo(...)\n    any = Any()\n    any.Pack(foo)\n    ...\n    if any.Is(Foo.DESCRIPTOR):\n      any.Unpack(foo)\n      ...\n\n Example 4: Pack and unpack a message in Go\n\n     foo := \u0026pb.Foo{...}\n     any, err := ptypes.MarshalAny(foo)\n     ...\n     foo := \u0026pb.Foo{}\n     if err := ptypes.UnmarshalAny(any, foo); err != nil {\n       ...\n     }\n\nThe pack methods provided by protobuf library will by default use\n'type.googleapis.com/full.type.name' as the type URL and the unpack\nmethods only use the fully qualified type name after the last '/'\nin the type URL, for example \"foo.bar.com/x/y.z\" will yield type\nname \"y.z\".\n\n\nJSON\n====\nThe JSON representation of an `Any` value uses the regular\nrepresentation of the deserialized, embedded message, with an\nadditional field `@type` which contains the type URL. Example:\n\n    package google.profile;\n    message Person {\n      string first_name = 1;\n      string last_name = 2;\n    }\n\n    {\n      \"@type\": \"type.googleapis.com/google.profile.Person\",\n      \"firstName\": \u003cstring\u003e,\n      \"lastName\": \u003cstring\u003e\n    }\n\nIf the embedded message type is well-known and has a custom JSON\nrepresentation, that representation will be embedded adding a field\n`value` which holds the custom JSON in addition to the `@type`\nfield. Example (for message [google.protobuf.Duration][]):\n\n    {\n      \"@type\": \"type.googleapis.com/google.protobuf.Duration\",\n      \"value\": \"1.212s\"\n    }"
    },
    "rpcStatus": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "details": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/protobufAny"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "ApiKeyAuth": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "BasicAuth": {
      "type": "basic"
    }
  },
  "security": [
    {
      "ApiKeyAuth": []
    }
  ]
}
