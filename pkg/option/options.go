package option

import (
	"github.com/go-redis/redis/v8"
	"github.com/jaycynth/farm-joint/internal/pkg/auth"
	"github.com/jaycynth/farm-joint/pkg/api/payment"
	"github.com/jaycynth/farm-joint/pkg/api/purchase"
	"github.com/jaycynth/farm-joint/pkg/api/sale"
	"google.golang.org/grpc/grpclog"
	"gorm.io/gorm"
)

// Options contains parameters needed by a service
type Options struct {
	SQLDB          *gorm.DB
	RedisDB        *redis.Client
	Logger         grpclog.LoggerV2
	APIHashKey     string
	PurchaseClient purchase.PurchaseAPIClient
	SaleClient     sale.SaleAPIClient
	PaymentClient  payment.PaymentAPIClient
	AuthAPI        auth.API
}
