// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package sale

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// SaleAPIClient is the client API for SaleAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SaleAPIClient interface {
	//make a sale
	CreateSale(ctx context.Context, in *CreateSaleRequest, opts ...grpc.CallOption) (*CreateSaleResponse, error)
	// Get a sale
	GetSale(ctx context.Context, in *GetSaleRequest, opts ...grpc.CallOption) (*Sale, error)
	// List  a sale
	ListSales(ctx context.Context, in *ListSalesRequest, opts ...grpc.CallOption) (*ListSalesResponse, error)
}

type saleAPIClient struct {
	cc grpc.ClientConnInterface
}

func NewSaleAPIClient(cc grpc.ClientConnInterface) SaleAPIClient {
	return &saleAPIClient{cc}
}

func (c *saleAPIClient) CreateSale(ctx context.Context, in *CreateSaleRequest, opts ...grpc.CallOption) (*CreateSaleResponse, error) {
	out := new(CreateSaleResponse)
	err := c.cc.Invoke(ctx, "/sale.SaleAPI/CreateSale", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *saleAPIClient) GetSale(ctx context.Context, in *GetSaleRequest, opts ...grpc.CallOption) (*Sale, error) {
	out := new(Sale)
	err := c.cc.Invoke(ctx, "/sale.SaleAPI/GetSale", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *saleAPIClient) ListSales(ctx context.Context, in *ListSalesRequest, opts ...grpc.CallOption) (*ListSalesResponse, error) {
	out := new(ListSalesResponse)
	err := c.cc.Invoke(ctx, "/sale.SaleAPI/ListSales", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SaleAPIServer is the server API for SaleAPI service.
// All implementations must embed UnimplementedSaleAPIServer
// for forward compatibility
type SaleAPIServer interface {
	//make a sale
	CreateSale(context.Context, *CreateSaleRequest) (*CreateSaleResponse, error)
	// Get a sale
	GetSale(context.Context, *GetSaleRequest) (*Sale, error)
	// List  a sale
	ListSales(context.Context, *ListSalesRequest) (*ListSalesResponse, error)
	mustEmbedUnimplementedSaleAPIServer()
}

// UnimplementedSaleAPIServer must be embedded to have forward compatible implementations.
type UnimplementedSaleAPIServer struct {
}

func (UnimplementedSaleAPIServer) CreateSale(context.Context, *CreateSaleRequest) (*CreateSaleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateSale not implemented")
}
func (UnimplementedSaleAPIServer) GetSale(context.Context, *GetSaleRequest) (*Sale, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSale not implemented")
}
func (UnimplementedSaleAPIServer) ListSales(context.Context, *ListSalesRequest) (*ListSalesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListSales not implemented")
}
func (UnimplementedSaleAPIServer) mustEmbedUnimplementedSaleAPIServer() {}

// UnsafeSaleAPIServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SaleAPIServer will
// result in compilation errors.
type UnsafeSaleAPIServer interface {
	mustEmbedUnimplementedSaleAPIServer()
}

func RegisterSaleAPIServer(s grpc.ServiceRegistrar, srv SaleAPIServer) {
	s.RegisterService(&_SaleAPI_serviceDesc, srv)
}

func _SaleAPI_CreateSale_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateSaleRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleAPIServer).CreateSale(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale.SaleAPI/CreateSale",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleAPIServer).CreateSale(ctx, req.(*CreateSaleRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SaleAPI_GetSale_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetSaleRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleAPIServer).GetSale(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale.SaleAPI/GetSale",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleAPIServer).GetSale(ctx, req.(*GetSaleRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SaleAPI_ListSales_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListSalesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleAPIServer).ListSales(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale.SaleAPI/ListSales",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleAPIServer).ListSales(ctx, req.(*ListSalesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _SaleAPI_serviceDesc = grpc.ServiceDesc{
	ServiceName: "sale.SaleAPI",
	HandlerType: (*SaleAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateSale",
			Handler:    _SaleAPI_CreateSale_Handler,
		},
		{
			MethodName: "GetSale",
			Handler:    _SaleAPI_GetSale_Handler,
		},
		{
			MethodName: "ListSales",
			Handler:    _SaleAPI_ListSales_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "sale.proto",
}
