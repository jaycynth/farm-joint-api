// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package feed

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// FeedAPIClient is the client API for FeedAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type FeedAPIClient interface {
	//Creates a feed
	CreateFeed(ctx context.Context, in *CreateFeedRequest, opts ...grpc.CallOption) (*Feed, error)
	//Update a feed
	UpdateFeed(ctx context.Context, in *UpdateFeedRequest, opts ...grpc.CallOption) (*Feed, error)
	// Gets a feed
	GetFeed(ctx context.Context, in *GetFeedRequest, opts ...grpc.CallOption) (*Feed, error)
	// List all feed
	ListFeed(ctx context.Context, in *ListFeedRequest, opts ...grpc.CallOption) (*ListFeedResponse, error)
	//buy a feed
	BuyFeed(ctx context.Context, in *BuyFeedRequest, opts ...grpc.CallOption) (*BuyFeedResponse, error)
}

type feedAPIClient struct {
	cc grpc.ClientConnInterface
}

func NewFeedAPIClient(cc grpc.ClientConnInterface) FeedAPIClient {
	return &feedAPIClient{cc}
}

func (c *feedAPIClient) CreateFeed(ctx context.Context, in *CreateFeedRequest, opts ...grpc.CallOption) (*Feed, error) {
	out := new(Feed)
	err := c.cc.Invoke(ctx, "/feed.FeedAPI/CreateFeed", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedAPIClient) UpdateFeed(ctx context.Context, in *UpdateFeedRequest, opts ...grpc.CallOption) (*Feed, error) {
	out := new(Feed)
	err := c.cc.Invoke(ctx, "/feed.FeedAPI/UpdateFeed", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedAPIClient) GetFeed(ctx context.Context, in *GetFeedRequest, opts ...grpc.CallOption) (*Feed, error) {
	out := new(Feed)
	err := c.cc.Invoke(ctx, "/feed.FeedAPI/GetFeed", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedAPIClient) ListFeed(ctx context.Context, in *ListFeedRequest, opts ...grpc.CallOption) (*ListFeedResponse, error) {
	out := new(ListFeedResponse)
	err := c.cc.Invoke(ctx, "/feed.FeedAPI/ListFeed", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedAPIClient) BuyFeed(ctx context.Context, in *BuyFeedRequest, opts ...grpc.CallOption) (*BuyFeedResponse, error) {
	out := new(BuyFeedResponse)
	err := c.cc.Invoke(ctx, "/feed.FeedAPI/BuyFeed", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FeedAPIServer is the server API for FeedAPI service.
// All implementations must embed UnimplementedFeedAPIServer
// for forward compatibility
type FeedAPIServer interface {
	//Creates a feed
	CreateFeed(context.Context, *CreateFeedRequest) (*Feed, error)
	//Update a feed
	UpdateFeed(context.Context, *UpdateFeedRequest) (*Feed, error)
	// Gets a feed
	GetFeed(context.Context, *GetFeedRequest) (*Feed, error)
	// List all feed
	ListFeed(context.Context, *ListFeedRequest) (*ListFeedResponse, error)
	//buy a feed
	BuyFeed(context.Context, *BuyFeedRequest) (*BuyFeedResponse, error)
	mustEmbedUnimplementedFeedAPIServer()
}

// UnimplementedFeedAPIServer must be embedded to have forward compatible implementations.
type UnimplementedFeedAPIServer struct {
}

func (UnimplementedFeedAPIServer) CreateFeed(context.Context, *CreateFeedRequest) (*Feed, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateFeed not implemented")
}
func (UnimplementedFeedAPIServer) UpdateFeed(context.Context, *UpdateFeedRequest) (*Feed, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateFeed not implemented")
}
func (UnimplementedFeedAPIServer) GetFeed(context.Context, *GetFeedRequest) (*Feed, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetFeed not implemented")
}
func (UnimplementedFeedAPIServer) ListFeed(context.Context, *ListFeedRequest) (*ListFeedResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListFeed not implemented")
}
func (UnimplementedFeedAPIServer) BuyFeed(context.Context, *BuyFeedRequest) (*BuyFeedResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BuyFeed not implemented")
}
func (UnimplementedFeedAPIServer) mustEmbedUnimplementedFeedAPIServer() {}

// UnsafeFeedAPIServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to FeedAPIServer will
// result in compilation errors.
type UnsafeFeedAPIServer interface {
	mustEmbedUnimplementedFeedAPIServer()
}

func RegisterFeedAPIServer(s grpc.ServiceRegistrar, srv FeedAPIServer) {
	s.RegisterService(&_FeedAPI_serviceDesc, srv)
}

func _FeedAPI_CreateFeed_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateFeedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedAPIServer).CreateFeed(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/feed.FeedAPI/CreateFeed",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedAPIServer).CreateFeed(ctx, req.(*CreateFeedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FeedAPI_UpdateFeed_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateFeedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedAPIServer).UpdateFeed(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/feed.FeedAPI/UpdateFeed",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedAPIServer).UpdateFeed(ctx, req.(*UpdateFeedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FeedAPI_GetFeed_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetFeedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedAPIServer).GetFeed(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/feed.FeedAPI/GetFeed",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedAPIServer).GetFeed(ctx, req.(*GetFeedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FeedAPI_ListFeed_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListFeedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedAPIServer).ListFeed(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/feed.FeedAPI/ListFeed",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedAPIServer).ListFeed(ctx, req.(*ListFeedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FeedAPI_BuyFeed_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BuyFeedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedAPIServer).BuyFeed(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/feed.FeedAPI/BuyFeed",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedAPIServer).BuyFeed(ctx, req.(*BuyFeedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _FeedAPI_serviceDesc = grpc.ServiceDesc{
	ServiceName: "feed.FeedAPI",
	HandlerType: (*FeedAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateFeed",
			Handler:    _FeedAPI_CreateFeed_Handler,
		},
		{
			MethodName: "UpdateFeed",
			Handler:    _FeedAPI_UpdateFeed_Handler,
		},
		{
			MethodName: "GetFeed",
			Handler:    _FeedAPI_GetFeed_Handler,
		},
		{
			MethodName: "ListFeed",
			Handler:    _FeedAPI_ListFeed_Handler,
		},
		{
			MethodName: "BuyFeed",
			Handler:    _FeedAPI_BuyFeed_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "feed.proto",
}
