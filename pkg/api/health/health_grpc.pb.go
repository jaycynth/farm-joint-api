// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package health

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// HealthAPIClient is the client API for HealthAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HealthAPIClient interface {
	//Creates a new Health
	CreateHealth(ctx context.Context, in *CreateHealthRequest, opts ...grpc.CallOption) (*Health, error)
	// Gets a Health using Health id
	GetHealth(ctx context.Context, in *GetHealthRequest, opts ...grpc.CallOption) (*Health, error)
	//List all health records
	ListHealth(ctx context.Context, in *ListHealthRequest, opts ...grpc.CallOption) (*ListHealthResponse, error)
}

type healthAPIClient struct {
	cc grpc.ClientConnInterface
}

func NewHealthAPIClient(cc grpc.ClientConnInterface) HealthAPIClient {
	return &healthAPIClient{cc}
}

func (c *healthAPIClient) CreateHealth(ctx context.Context, in *CreateHealthRequest, opts ...grpc.CallOption) (*Health, error) {
	out := new(Health)
	err := c.cc.Invoke(ctx, "/health.HealthAPI/CreateHealth", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *healthAPIClient) GetHealth(ctx context.Context, in *GetHealthRequest, opts ...grpc.CallOption) (*Health, error) {
	out := new(Health)
	err := c.cc.Invoke(ctx, "/health.HealthAPI/GetHealth", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *healthAPIClient) ListHealth(ctx context.Context, in *ListHealthRequest, opts ...grpc.CallOption) (*ListHealthResponse, error) {
	out := new(ListHealthResponse)
	err := c.cc.Invoke(ctx, "/health.HealthAPI/ListHealth", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HealthAPIServer is the server API for HealthAPI service.
// All implementations must embed UnimplementedHealthAPIServer
// for forward compatibility
type HealthAPIServer interface {
	//Creates a new Health
	CreateHealth(context.Context, *CreateHealthRequest) (*Health, error)
	// Gets a Health using Health id
	GetHealth(context.Context, *GetHealthRequest) (*Health, error)
	//List all health records
	ListHealth(context.Context, *ListHealthRequest) (*ListHealthResponse, error)
	mustEmbedUnimplementedHealthAPIServer()
}

// UnimplementedHealthAPIServer must be embedded to have forward compatible implementations.
type UnimplementedHealthAPIServer struct {
}

func (UnimplementedHealthAPIServer) CreateHealth(context.Context, *CreateHealthRequest) (*Health, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateHealth not implemented")
}
func (UnimplementedHealthAPIServer) GetHealth(context.Context, *GetHealthRequest) (*Health, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetHealth not implemented")
}
func (UnimplementedHealthAPIServer) ListHealth(context.Context, *ListHealthRequest) (*ListHealthResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListHealth not implemented")
}
func (UnimplementedHealthAPIServer) mustEmbedUnimplementedHealthAPIServer() {}

// UnsafeHealthAPIServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HealthAPIServer will
// result in compilation errors.
type UnsafeHealthAPIServer interface {
	mustEmbedUnimplementedHealthAPIServer()
}

func RegisterHealthAPIServer(s grpc.ServiceRegistrar, srv HealthAPIServer) {
	s.RegisterService(&_HealthAPI_serviceDesc, srv)
}

func _HealthAPI_CreateHealth_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateHealthRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HealthAPIServer).CreateHealth(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/health.HealthAPI/CreateHealth",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HealthAPIServer).CreateHealth(ctx, req.(*CreateHealthRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HealthAPI_GetHealth_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetHealthRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HealthAPIServer).GetHealth(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/health.HealthAPI/GetHealth",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HealthAPIServer).GetHealth(ctx, req.(*GetHealthRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HealthAPI_ListHealth_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListHealthRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HealthAPIServer).ListHealth(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/health.HealthAPI/ListHealth",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HealthAPIServer).ListHealth(ctx, req.(*ListHealthRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _HealthAPI_serviceDesc = grpc.ServiceDesc{
	ServiceName: "health.HealthAPI",
	HandlerType: (*HealthAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateHealth",
			Handler:    _HealthAPI_CreateHealth_Handler,
		},
		{
			MethodName: "GetHealth",
			Handler:    _HealthAPI_GetHealth_Handler,
		},
		{
			MethodName: "ListHealth",
			Handler:    _HealthAPI_ListHealth_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "health.proto",
}
