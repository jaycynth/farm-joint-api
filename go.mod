module github.com/jaycynth/farm-joint

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gidyon/micro v1.11.1
	github.com/go-redis/redis/v8 v8.4.2
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.0.1
	github.com/lib/pq v1.9.0
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/speps/go-hashids v2.0.0+incompatible
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/sys v0.0.0-20210105210732-16f7687f5001 // indirect
	golang.org/x/tools v0.0.0-20201208183658-cc330816fc52 // indirect
	google.golang.org/genproto v0.0.0-20210106152847-07624b53cd92
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
	gorm.io/gorm v1.20.8
)
